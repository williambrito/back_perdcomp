/*
 * To change this license header, choose License Headers in Project FileProperties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.taxationmind.fileutils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nathalia Cruz
 */
public class FileProperties {

    private static FileProperties fileProperties = null;
    private static final Properties PROPERTIES = new Properties();
    private final Path path;
    public static final String PROPERTY_FILE_NAME = "livros.properties";
    private static final Map<String, String> propertiesMap = new HashMap<>();

    private FileProperties(String p) {
        path = Paths.get(p + PROPERTY_FILE_NAME);
        if (createPropertyFile(p)) {
            load();
        }
    }

    public static FileProperties getInstance() {
        if (fileProperties == null) {
            fileProperties = new FileProperties("./");
        }

        return fileProperties;
    }

    public static FileProperties getNewInstance(String p) {
        fileProperties = new FileProperties(p);

        return fileProperties;
    }

    private boolean createPropertyFile(String p) {
        boolean exist = true;

        if (!Files.exists(path)) {
            Logger.getLogger(Properties.class.getName()).log(Level.INFO, "Creating File Properties in {0}", path);

            try ( OutputStream os = new FileOutputStream(path.toFile())) {
                PROPERTIES.setProperty("DB_URL", "//192.168.1.3:5432/homologacao_nfe_old");
                PROPERTIES.setProperty("DB_USER", "postgres");
                PROPERTIES.setProperty("DB_PASSWD", "");
                PROPERTIES.setProperty("HOST", "0.0.0.0");
                PROPERTIES.setProperty("HTTP_PORT", "7002");
                PROPERTIES.setProperty("HTTPS_PORT", "7443");
                PROPERTIES.setProperty("AMBIENTE", "H");
                PROPERTIES.setProperty("DEV_MODE", "0");

                PROPERTIES.store(os, p);
                Logger.getLogger(Properties.class.getName()).log(Level.INFO, "Created File Properties...");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Properties.class.getName()).log(Level.SEVERE, "File Properties Not Found in {0}", path);
                exist = false;
            } catch (IOException ex) {
                Logger.getLogger(Properties.class.getName()).log(Level.SEVERE, "Error Creating File Properties in {0}", path);
                exist = false;
            }

        } else {
            Logger.getLogger(Properties.class.getName()).log(Level.INFO, "File Properties Exist in {0}", path);
        }

        return exist;
    }

    private void load() {
        try ( InputStream in = new FileInputStream(path.toFile())) {
            PROPERTIES.load(in);
            propertiesToMap();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Properties.class.getName()).log(Level.SEVERE, "File Properties Not Found in {0}", path);
        } catch (IOException ex) {
            Logger.getLogger(Properties.class.getName()).log(Level.SEVERE, "Error Loading File Properties in {0}", path);
        }
    }

    public void refresh() {
        load();
    }

    public String getProperty(String key) {
        String value = "";

        if (PROPERTIES.isEmpty()) {
            Logger.getLogger(Properties.class.getName()).log(Level.WARNING, "Properties File is empty");
        } else if (!PROPERTIES.containsKey(key)) {
            Logger.getLogger(Properties.class.getName()).log(Level.WARNING, "Key {0} not exist in {1}", new Object[]{key, path});
        } else {
            value = PROPERTIES.getProperty(key);
        }

        return value;
    }

    public List<String> getListKey() {
        List list = Collections.list(PROPERTIES.keys());

        return list;
    }

    private void propertiesToMap() {
        if (!PROPERTIES.isEmpty()) {
            PROPERTIES.stringPropertyNames().forEach(name
                    -> propertiesMap.put(name, PROPERTIES.getProperty(name))
            );
        }
    }

    public Map<String, String> getMap() {
        return propertiesMap;
    }
}
