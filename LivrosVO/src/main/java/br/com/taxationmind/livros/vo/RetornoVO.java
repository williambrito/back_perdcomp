/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.taxationmind.livros.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 *
 * @author William Brito
 */
@JsonPropertyOrder({ "type", "success", "message", "error" })
public class RetornoVO<T> {
    
    private String type;
    private boolean success;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Object message;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private String error;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T value;

    public RetornoVO(String type, boolean success, Object message, String error) {
        this.type = type;
        this.success = success;
        this.message = message;
        this.error = error;
    }
    
    public RetornoVO(String type, boolean success, Object message, String error, T value) {
        this.type = type;
        this.success = success;
        this.message = message;
        this.error = error;
        this.value = value;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Object getMessage() {
        return message;
    }

    public void setMessage(Object message) {
        this.message = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }
}
