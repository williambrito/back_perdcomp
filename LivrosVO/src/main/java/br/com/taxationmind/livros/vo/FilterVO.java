/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package br.com.taxationmind.livros.vo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;
import org.apache.commons.validator.GenericValidator;

/**
 *
 * @author william brito
 */
public class FilterVO {

    private String field;
    private int tipo;
    private String value;
    private String clause = null;

    public FilterVO(String field, int tipo) {
        this.field = field;
        this.tipo = tipo;
    }

    public FilterVO(String field, int tipo, String value) {
        this(field, tipo);
        this.value = value;
    }

    public FilterVO(String field, int tipo, String value, String clause) {
        this(field, tipo, value);
        this.clause = clause;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getClause() {
        return clause;
    }

    public void setClause(String clause) {
        this.clause = clause;
    }
public Object convertValue() {

        if (tipo == java.sql.Types.DATE) {
            String[] tmpData = value.split(":");
            java.sql.Date[] data = new java.sql.Date[tmpData.length];

            for (int i = 0; i < tmpData.length; i++) {
                if (GenericValidator.isDate(tmpData[i], "yyyy-MM-dd", true)) {
                    try {
                        Date d = new SimpleDateFormat("yyyy-MM-dd").parse(tmpData[i]);
                        data[i] = new java.sql.Date(d.getTime());
                    } catch (ParseException ex) {
                        Logger.getLogger(ex.getMessage());
                    }
                }
            }
            return data.length == 1 ? data[0] : data;

        } else if (tipo == java.sql.Types.TIMESTAMP) {
            if (GenericValidator.isDate(value, "yyyy-MM-dd HH:mm:ss", true)) {
                try {
                    Date d = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(value);
                    return new java.sql.Timestamp(d.getTime());
                } catch (ParseException ex) {
                   Logger.getLogger(ex.getMessage());
                }
            }
        } else if (tipo == java.sql.Types.BIGINT) {
            String[] tmpNumber = value.split(":");
            Long[] number = new Long[tmpNumber.length];

            for (int i = 0; i < tmpNumber.length; i++) {
                try {
                    number[i] = Long.valueOf(tmpNumber[i]);
                } catch (NumberFormatException ex) {
                    number[i] = 0L;
                }
            }
            return number.length == 1 ? number[0] : number;
        } else if (tipo == java.sql.Types.INTEGER) {
            String[] tmpNumber = value.split(":");
            Integer[] number = new Integer[tmpNumber.length];

            for (int i = 0; i < tmpNumber.length; i++) {
                try {
                    number[i] = Integer.valueOf(tmpNumber[i]);
                } catch (NumberFormatException ex) {
                    number[i] = 0;
                }
            }
            return number.length == 1 ? number[0] : number;
        } else if (tipo == java.sql.Types.NUMERIC) {
            String[] tmpNumber = value.split(":");
            Double[] number = new Double[tmpNumber.length];

            for (int i = 0; i < tmpNumber.length; i++) {
                try {
                    number[i] = Double.valueOf(tmpNumber[i]);
                } catch (NumberFormatException ex) {
                    number[i] = 0D;
                }
            }
            return number.length == 1 ? number[0] : number;
        } else if (tipo == java.sql.Types.ARRAY) {
            String[] tmp = value.split(":");

            return tmp;
        } else if (tipo == java.sql.Types.BOOLEAN) {
            return value.equals("true");
        }

        return value;
    }

}