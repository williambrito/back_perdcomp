/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class CadastroR15VO {

    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r15", new FilterVO("r15", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("cnpj_detentor", new FilterVO("cnpj_detentor", Types.VARCHAR));
        FILTER_MAP.put("cnpj_emitente", new FilterVO("cnpj_emitente", Types.VARCHAR));
        FILTER_MAP.put("num_nota", new FilterVO("num_nota", Types.VARCHAR));
        FILTER_MAP.put("serie", new FilterVO("serie", Types.VARCHAR));
        FILTER_MAP.put("data_emissao", new FilterVO("data_emissao", Types.VARCHAR));
        FILTER_MAP.put("data_entrada", new FilterVO("data_entrada", Types.VARCHAR));
        FILTER_MAP.put("cfop", new FilterVO("cfop", Types.VARCHAR));
        FILTER_MAP.put("valor_total", new FilterVO("valor_total", Types.VARCHAR));
        FILTER_MAP.put("valor_ipi_destacado", new FilterVO("valor_ipi_destacado", Types.VARCHAR));
        FILTER_MAP.put("valor_ipi_creditado", new FilterVO("valor_ipi_creditado", Types.VARCHAR));
        FILTER_MAP.put("especie_cred", new FilterVO("especie_cred", Types.VARCHAR));
        FILTER_MAP.put("decendio", new FilterVO("decendio", Types.VARCHAR));
        FILTER_MAP.put("mes_periodo", new FilterVO("mes_periodo", Types.VARCHAR));
        FILTER_MAP.put("ano_periodo", new FilterVO("ano_periodo", Types.VARCHAR));

    }
    private Long id;
    private String r15;
    private String cnpj_declarante;
    private String cnpj_sucedida;
    private String cnpj_detentor;
    private String cnpj_emitente;
    private String num_nota;
    private String serie;
    private String data_emissao;
    private String data_entrada;
    private String cfop;
    private String valor_total;
    private String valor_ipi_destacado;
    private String valor_ipi_creditado;
    private String especie_cred;
    private String decendio;
    private String mes_periodo;
    private String ano_periodo;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r15
     */
    public String getR15() {
        return r15;
    }

    /**
     * @param r15 the r15 to set
     */
    public void setR15(String r15) {
        this.r15 = r15;
    }

    /**
     * @return the cnpj_declarante
     */
    public String getCnpj_declarante() {
        return cnpj_declarante;
    }

    /**
     * @param cnpj_declarante the cnpj_declarante to set
     */
    public void setCnpj_declarante(String cnpj_declarante) {
        this.cnpj_declarante = cnpj_declarante;
    }

    /**
     * @return the cnpj_sucedida
     */
    public String getCnpj_sucedida() {
        return cnpj_sucedida;
    }

    /**
     * @param cnpj_sucedida the cnpj_sucedida to set
     */
    public void setCnpj_sucedida(String cnpj_sucedida) {
        this.cnpj_sucedida = cnpj_sucedida;
    }

    /**
     * @return the cnpj_detentor
     */
    public String getCnpj_detentor() {
        return cnpj_detentor;
    }

    /**
     * @param cnpj_detentor the cnpj_detentor to set
     */
    public void setCnpj_detentor(String cnpj_detentor) {
        this.cnpj_detentor = cnpj_detentor;
    }

    /**
     * @return the cnpj_emitente
     */
    public String getCnpj_emitente() {
        return cnpj_emitente;
    }

    /**
     * @param cnpj_emitente the cnpj_emitente to set
     */
    public void setCnpj_emitente(String cnpj_emitente) {
        this.cnpj_emitente = cnpj_emitente;
    }

    /**
     * @return the num_nota
     */
    public String getNum_nota() {
        return num_nota;
    }

    /**
     * @param num_nota the num_nota to set
     */
    public void setNum_nota(String num_nota) {
        this.num_nota = num_nota;
    }

    /**
     * @return the serie
     */
    public String getSerie() {
        return serie;
    }

    /**
     * @param serie the serie to set
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }

    /**
     * @return the data_emissao
     */
    public String getData_emissao() {
        return data_emissao;
    }

    /**
     * @param data_emissao the data_emissao to set
     */
    public void setData_emissao(String data_emissao) {
        this.data_emissao = data_emissao;
    }

    /**
     * @return the data_entrada
     */
    public String getData_entrada() {
        return data_entrada;
    }

    /**
     * @param data_entrada the data_entrada to set
     */
    public void setData_entrada(String data_entrada) {
        this.data_entrada = data_entrada;
    }

    /**
     * @return the cfop
     */
    public String getCfop() {
        return cfop;
    }

    /**
     * @param cfop the cfop to set
     */
    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    /**
     * @return the valor_total
     */
    public String getValor_total() {
        return valor_total;
    }

    /**
     * @param valor_total the valor_total to set
     */
    public void setValor_total(String valor_total) {
        this.valor_total = valor_total;
    }

    /**
     * @return the valor_ipi_destacado
     */
    public String getValor_ipi_destacado() {
        return valor_ipi_destacado;
    }

    /**
     * @param valor_ipi_destacado the valor_ipi_destacado to set
     */
    public void setValor_ipi_destacado(String valor_ipi_destacado) {
        this.valor_ipi_destacado = valor_ipi_destacado;
    }

    /**
     * @return the valor_ipi_creditado
     */
    public String getValor_ipi_creditado() {
        return valor_ipi_creditado;
    }

    /**
     * @param valor_ipi_creditado the valor_ipi_creditado to set
     */
    public void setValor_ipi_creditado(String valor_ipi_creditado) {
        this.valor_ipi_creditado = valor_ipi_creditado;
    }

    /**
     * @return the especie_cred
     */
    public String getEspecie_cred() {
        return especie_cred;
    }

    /**
     * @param especie_cred the especie_cred to set
     */
    public void setEspecie_cred(String especie_cred) {
        this.especie_cred = especie_cred;
    }

    /**
     * @return the decendio
     */
    public String getDecendio() {
        return decendio;
    }

    /**
     * @param decendio the decendio to set
     */
    public void setDecendio(String decendio) {
        this.decendio = decendio;
    }

    /**
     * @return the mes_periodo
     */
    public String getMes_periodo() {
        return mes_periodo;
    }

    /**
     * @param mes_periodo the mes_periodo to set
     */
    public void setMes_periodo(String mes_periodo) {
        this.mes_periodo = mes_periodo;
    }

    /**
     * @return the ano_periodo
     */
    public String getAno_periodo() {
        return ano_periodo;
    }

    /**
     * @param ano_periodo the ano_periodo to set
     */
    public void setAno_periodo(String ano_periodo) {
        this.ano_periodo = ano_periodo;
    }

}
