/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class BookDataVO {

    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("book_id", new FilterVO("book_id", Types.BIGINT));
        FILTER_MAP.put("data", new FilterVO("data", Types.VARCHAR));
        FILTER_MAP.put("status", new FilterVO("status", Types.BOOLEAN));

    }

    private Long id;
    private Long book_id;
    private Object data;
    private Boolean status;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the book_id
     */
    public Long getBook_id() {
        return book_id;
    }

    /**
     * @param book_id the book_id to set
     */
    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    /**
     * @return the data
     */
    public Object getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(Object data) {
        this.data = data;
    }

    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

}
