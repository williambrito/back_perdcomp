/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import java.util.Map;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.HashMap;
import java.sql.Types;
import br.com.taxationmind.livros.vo.FilterVO;

/**
 *
 * @author William Brito
 */
public class LivrosVO {

    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("book_name", new FilterVO("book_name", Types.VARCHAR));
        FILTER_MAP.put("status", new FilterVO("status", Types.BOOLEAN));

    }

    private Long id;
    private String book_name;
    private Boolean status;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the book_name
     */
    public String getBook_name() {
        return book_name;
    }

    /**
     * @param book_name the book_name to set
     */
    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    /**
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * @return the creation_dt
     */
}
