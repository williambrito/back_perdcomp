/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class CadastroR21VO {

    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r21", new FilterVO("r21", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("cnpj_detentor", new FilterVO("cnpj_detentor", Types.VARCHAR));
        FILTER_MAP.put("ano_apuracao", new FilterVO("ano_apuracao", Types.VARCHAR));
        FILTER_MAP.put("mes_apuracao", new FilterVO("mes_apuracao", Types.VARCHAR));
        FILTER_MAP.put("decendio", new FilterVO("decendio", Types.VARCHAR));
        FILTER_MAP.put("movimento", new FilterVO("movimento", Types.VARCHAR));
        FILTER_MAP.put("credito_nacional", new FilterVO("credito_nacional", Types.VARCHAR));
        FILTER_MAP.put("credito_externo", new FilterVO("credito_externo", Types.VARCHAR));
        FILTER_MAP.put("credito_estorno", new FilterVO("credito_estorno", Types.VARCHAR));
        FILTER_MAP.put("credito_presumido", new FilterVO("credito_presumido", Types.VARCHAR));
        FILTER_MAP.put("credito_extemporaneos", new FilterVO("credito_extemporaneos", Types.VARCHAR));
        FILTER_MAP.put("credito_demais", new FilterVO("credito_demais", Types.VARCHAR));
        FILTER_MAP.put("debito_nacional", new FilterVO("debito_nacional", Types.VARCHAR));
        FILTER_MAP.put("debito_estorno", new FilterVO("debito_estorno", Types.VARCHAR));
        FILTER_MAP.put("demonstrativo_ressarcimento", new FilterVO("demonstrativo_ressarcimento", Types.VARCHAR));
        FILTER_MAP.put("demonstrativo_debitos", new FilterVO("demonstrativo_debitos", Types.VARCHAR));

    }

    private Long id;
    private String r21;
    private String cnpjDeclarante;
    private String cnpjSucedida;
    private String cnpjDetentor;
    private String anoApuracao;
    private String mesApuracao;
    private String decendio;
    private String movimento;
    private String creditoNacional;
    private String creditoExterno;
    private String creditoEstorno;
    private String creditoPresumido;
    private String creditoExtemporaneos;
    private String creditoDemais;
    private String debitoNacional;
    private String debitoEstorno;
    private String demonstrativoRessarcimento;
    private String demonstrativoDebitos;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r21
     */
    public String getR21() {
        return r21;
    }

    /**
     * @param r21 the r21 to set
     */
    public void setR21(String r21) {
        this.r21 = r21;
    }

    /**
     * @return the cnpjDeclarante
     */
    public String getCnpjDeclarante() {
        return cnpjDeclarante;
    }

    /**
     * @param cnpjDeclarante the cnpjDeclarante to set
     */
    public void setCnpjDeclarante(String cnpjDeclarante) {
        this.cnpjDeclarante = cnpjDeclarante;
    }

    /**
     * @return the cnpjSucedida
     */
    public String getCnpjSucedida() {
        return cnpjSucedida;
    }

    /**
     * @param cnpjSucedida the cnpjSucedida to set
     */
    public void setCnpjSucedida(String cnpjSucedida) {
        this.cnpjSucedida = cnpjSucedida;
    }

    /**
     * @return the cnpjDetentor
     */
    public String getCnpjDetentor() {
        return cnpjDetentor;
    }

    /**
     * @param cnpjDetentor the cnpjDetentor to set
     */
    public void setCnpjDetentor(String cnpjDetentor) {
        this.cnpjDetentor = cnpjDetentor;
    }

    /**
     * @return the ano_apuracao
     */
    public String getAnoApuracao() {
        return anoApuracao;
    }

    /**
     * @param anoApuracao the ano_apuracao to set
     */
    public void setAnoApuracao(String anoApuracao) {
        this.anoApuracao = anoApuracao;
    }

    /**
     * @return the mesApuracao
     */
    public String getMesApuracao() {
        return mesApuracao;
    }

    /**
     * @param mesApuracao the mesApuracao to set
     */
    public void setMesApuracao(String mesApuracao) {
        this.mesApuracao = mesApuracao;
    }

    /**
     * @return the decendio
     */
    public String getDecendio() {
        return decendio;
    }

    /**
     * @param decendio the decendio to set
     */
    public void setDecendio(String decendio) {
        this.decendio = decendio;
    }

    /**
     * @return the movimento
     */
    public String getMovimento() {
        return movimento;
    }

    /**
     * @param movimento the movimento to set
     */
    public void setMovimento(String movimento) {
        this.movimento = movimento;
    }

    /**
     * @return the creditoNacional
     */
    public String getCreditoNacional() {
        return creditoNacional;
    }

    /**
     * @param creditoNacional the creditoNacional to set
     */
    public void setCreditoNacional(String creditoNacional) {
        this.creditoNacional = creditoNacional;
    }

    /**
     * @return the creditoExterno
     */
    public String getCreditoExterno() {
        return creditoExterno;
    }

    /**
     * @param creditoExterno the creditoExterno to set
     */
    public void setCreditoExterno(String creditoExterno) {
        this.creditoExterno = creditoExterno;
    }

    /**
     * @return the creditoEstorno
     */
    public String getCreditoEstorno() {
        return creditoEstorno;
    }

    /**
     * @param creditoEstorno the creditoEstorno to set
     */
    public void setCreditoEstorno(String creditoEstorno) {
        this.creditoEstorno = creditoEstorno;
    }

    /**
     * @return the creditoPresumido
     */
    public String getCreditoPresumido() {
        return creditoPresumido;
    }

    /**
     * @param creditoPresumido the creditoPresumido to set
     */
    public void setCreditoPresumido(String creditoPresumido) {
        this.creditoPresumido = creditoPresumido;
    }

    /**
     * @return the creditoExtemporaneos
     */
    public String getCreditoExtemporaneos() {
        return creditoExtemporaneos;
    }

    /**
     * @param creditoExtemporaneos the creditoExtemporaneos to set
     */
    public void setCreditoExtemporaneos(String creditoExtemporaneos) {
        this.creditoExtemporaneos = creditoExtemporaneos;
    }

    /**
     * @return the creditoDemais
     */
    public String getCreditoDemais() {
        return creditoDemais;
    }

    /**
     * @param creditoDemais the creditoDemais to set
     */
    public void setCreditoDemais(String creditoDemais) {
        this.creditoDemais = creditoDemais;
    }

    /**
     * @return the debitoNacional
     */
    public String getDebitoNacional() {
        return debitoNacional;
    }

    /**
     * @param debitoNacional the debitoNacional to set
     */
    public void setDebitoNacional(String debitoNacional) {
        this.debitoNacional = debitoNacional;
    }

    /**
     * @return the debitoEstorno
     */
    public String getDebitoEstorno() {
        return debitoEstorno;
    }

    /**
     * @param debitoEstorno the debitoEstorno to set
     */
    public void setDebitoEstorno(String debitoEstorno) {
        this.debitoEstorno = debitoEstorno;
    }

    /**
     * @return the demonstrativoRessarcimento
     */
    public String getDemonstrativoRessarcimento() {
        return demonstrativoRessarcimento;
    }

    /**
     * @param demonstrativoRessarcimento the demonstrativoRessarcimento to set
     */
    public void setDemonstrativoRessarcimento(String demonstrativoRessarcimento) {
        this.demonstrativoRessarcimento = demonstrativoRessarcimento;
    }

    /**
     * @return the demonstrativoDebitos
     */
    public String getDemonstrativoDebitos() {
        return demonstrativoDebitos;
    }

    /**
     * @param demonstrativoDebitos the demonstrativoDebitos to set
     */
    public void setDemonstrativoDebitos(String demonstrativoDebitos) {
        this.demonstrativoDebitos = demonstrativoDebitos;
    }

}
