/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

public class CadastroR11VO {

    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r11", new FilterVO("r11", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("cnpj_detentor", new FilterVO("cnpj_detentor", Types.VARCHAR));
        FILTER_MAP.put("ano_apuracao", new FilterVO("ano_apuracao", Types.VARCHAR));
        FILTER_MAP.put("mes_apuracao", new FilterVO("mes_apuracao", Types.VARCHAR));
        FILTER_MAP.put("decendio", new FilterVO("decendio", Types.VARCHAR));
        FILTER_MAP.put("cfop", new FilterVO("cfop", Types.VARCHAR));
        FILTER_MAP.put("base_calculo", new FilterVO("base_calculo", Types.VARCHAR));
        FILTER_MAP.put("ipi_creditado", new FilterVO("ipi_creditado", Types.VARCHAR));
        FILTER_MAP.put("isentas_nao_tributadas", new FilterVO("isentas_nao_tributadas", Types.VARCHAR));
        FILTER_MAP.put("outras", new FilterVO("outras", Types.VARCHAR));

    }

    private Long id;
    private String r11;
    private String cnpjDeclarante;
    private String cnpjSucedida;
    private String cnpjDetentor;
    private String anoApuracao;
    private String mesApuracao;
    private String decendio;
    private String cfop;
    private String baseCalculo;
    private String ipiCreditado;
    private String isentasNaotributada;
    private String outras;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r11
     */
    public String getR11() {
        return r11;
    }

    /**
     * @param r11 the r11 to set
     */
    public void setR11(String r11) {
        this.r11 = r11;
    }

    /**
     * @return the cnpjDeclarante
     */
    public String getCnpjDeclarante() {
        return cnpjDeclarante;
    }

    /**
     * @param cnpjDeclarante the cnpjDeclarante to set
     */
    public void setCnpjDeclarante(String cnpjDeclarante) {
        this.cnpjDeclarante = cnpjDeclarante;
    }

    /**
     * @return the cnpjSucedida
     */
    public String getCnpjSucedida() {
        return cnpjSucedida;
    }

    /**
     * @param cnpjSucedida the cnpjSucedida to set
     */
    public void setCnpjSucedida(String cnpjSucedida) {
        this.cnpjSucedida = cnpjSucedida;
    }

    /**
     * @return the cnpjDetentor
     */
    public String getCnpjDetentor() {
        return cnpjDetentor;
    }

    /**
     * @param cnpjDetentor the cnpjDetentor to set
     */
    public void setCnpjDetentor(String cnpjDetentor) {
        this.cnpjDetentor = cnpjDetentor;
    }

    /**
     * @return the anoApuracao
     */
    public String getAnoApuracao() {
        return anoApuracao;
    }

    /**
     * @param anoApuracao the anoApuracao to set
     */
    public void setAnoApuracao(String anoApuracao) {
        this.anoApuracao = anoApuracao;
    }

    /**
     * @return the mesApuracao
     */
    public String getMesApuracao() {
        return mesApuracao;
    }

    /**
     * @param mesApuracao the mesApuracao to set
     */
    public void setMesApuracao(String mesApuracao) {
        this.mesApuracao = mesApuracao;
    }

    /**
     * @return the decendio
     */
    public String getDecendio() {
        return decendio;
    }

    /**
     * @param decendio the decendio to set
     */
    public void setDecendio(String decendio) {
        this.decendio = decendio;
    }

    /**
     * @return the cfop
     */
    public String getCfop() {
        return cfop;
    }

    /**
     * @param cfop the cfop to set
     */
    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    /**
     * @return the baseCalculo
     */
    public String getBaseCalculo() {
        return baseCalculo;
    }

    /**
     * @param baseCalculo the baseCalculo to set
     */
    public void setBaseCalculo(String baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    /**
     * @return the ipiCreditado
     */
    public String getIpiCreditado() {
        return ipiCreditado;
    }

    /**
     * @param ipiCreditado the ipiCreditado to set
     */
    public void setIpiCreditado(String ipiCreditado) {
        this.ipiCreditado = ipiCreditado;
    }

    /**
     * @return the isentasNaotributada
     */
    public String getIsentasNaotributada() {
        return isentasNaotributada;
    }

    /**
     * @param isentasNaotributada the isentasNaotributada to set
     */
    public void setIsentasNaotributada(String isentasNaotributada) {
        this.isentasNaotributada = isentasNaotributada;
    }

    /**
     * @return the outras
     */
    public String getOutras() {
        return outras;
    }

    /**
     * @param outras the outras to set
     */
    public void setOutras(String outras) {
        this.outras = outras;
    }

}
