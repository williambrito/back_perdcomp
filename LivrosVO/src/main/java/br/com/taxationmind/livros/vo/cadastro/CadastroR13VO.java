/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class CadastroR13VO {
    
    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();
    
     static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r13", new FilterVO("r13", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("cnpj_detentor", new FilterVO("cnpj_detentor", Types.VARCHAR));
        FILTER_MAP.put("cnpj_emitente",new FilterVO("cnpj_emitente",Types.VARCHAR));
        FILTER_MAP.put("num_nota", new FilterVO("num_nota", Types.VARCHAR));
        FILTER_MAP.put("serie", new FilterVO("serie", Types.VARCHAR));
        FILTER_MAP.put("data_emissao", new FilterVO("data_emissao", Types.VARCHAR));
        FILTER_MAP.put("data_entrada", new FilterVO("data_entrada", Types.VARCHAR));
        FILTER_MAP.put("cfop", new FilterVO("cfop", Types.VARCHAR));
        FILTER_MAP.put("valor_total", new FilterVO("valor_total", Types.VARCHAR));
        FILTER_MAP.put("valor_ipi_destacado", new FilterVO("valor_ipi_destacado", Types.VARCHAR));
        FILTER_MAP.put("valor_ipi_creditado", new FilterVO("valor_ipi_creditado", Types.VARCHAR));
        

    }
    
    private Long id;
    private String r13;
    private String cnpjDeclarante;
    private String cnpjSucedida;
    private String cnpjDetentor;
    private String cnpjEmitente;
    private String numNota;
    private String serie;
    private String dataEmissao;
    private String dataEntrada;
    private String cfop;
    private String valorTotal;
    private String valorIpiDestacado;
    private String valorIpiCreditado;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r13
     */
    public String getR13() {
        return r13;
    }

    /**
     * @param r13 the r13 to set
     */
    public void setR13(String r13) {
        this.r13 = r13;
    }

    /**
     * @return the cnpjDeclarante
     */
    public String getCnpjDeclarante() {
        return cnpjDeclarante;
    }

    /**
     * @param cnpjDeclarante the cnpjDeclarante to set
     */
    public void setCnpjDeclarante(String cnpjDeclarante) {
        this.cnpjDeclarante = cnpjDeclarante;
    }

    /**
     * @return the cnpjSucedida
     */
    public String getCnpjSucedida() {
        return cnpjSucedida;
    }

    /**
     * @param cnpjSucedida the cnpjSucedida to set
     */
    public void setCnpjSucedida(String cnpjSucedida) {
        this.cnpjSucedida = cnpjSucedida;
    }

    /**
     * @return the cnpjDetentor
     */
    public String getCnpjDetentor() {
        return cnpjDetentor;
    }

    /**
     * @param cnpjDetentor the cnpjDetentor to set
     */
    public void setCnpjDetentor(String cnpjDetentor) {
        this.cnpjDetentor = cnpjDetentor;
    }

    /**
     * @return the cnpjEmitente
     */
    public String getCnpjEmitente() {
        return cnpjEmitente;
    }

    /**
     * @param cnpjEmitente the cnpjEmitente to set
     */
    public void setCnpjEmitente(String cnpjEmitente) {
        this.cnpjEmitente = cnpjEmitente;
    }

    /**
     * @return the numNota
     */
    public String getNumNota() {
        return numNota;
    }

    /**
     * @param numNota the numNota to set
     */
    public void setNumNota(String numNota) {
        this.numNota = numNota;
    }

    /**
     * @return the serie
     */
    public String getSerie() {
        return serie;
    }

    /**
     * @param serie the serie to set
     */
    public void setSerie(String serie) {
        this.serie = serie;
    }

    /**
     * @return the dataEmissao
     */
    public String getDataEmissao() {
        return dataEmissao;
    }

    /**
     * @param dataEmissao the dataEmissao to set
     */
    public void setDataEmissao(String dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    /**
     * @return the dataEntrada
     */
    public String getDataEntrada() {
        return dataEntrada;
    }

    /**
     * @param dataEntrada the dataEntrada to set
     */
    public void setDataEntrada(String dataEntrada) {
        this.dataEntrada = dataEntrada;
    }

    /**
     * @return the cfop
     */
    public String getCfop() {
        return cfop;
    }

    /**
     * @param cfop the cfop to set
     */
    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    /**
     * @return the valorTotal
     */
    public String getValorTotal() {
        return valorTotal;
    }

    /**
     * @param valorTotal the valorTotal to set
     */
    public void setValorTotal(String valorTotal) {
        this.valorTotal = valorTotal;
    }

    /**
     * @return the valorIpiDestacado
     */
    public String getValorIpiDestacado() {
        return valorIpiDestacado;
    }

    /**
     * @param valorIpiDestacado the valorIpiDestacado to set
     */
    public void setValorIpiDestacado(String valorIpiDestacado) {
        this.valorIpiDestacado = valorIpiDestacado;
    }

    /**
     * @return the valorIpiCreditado
     */
    public String getValorIpiCreditado() {
        return valorIpiCreditado;
    }

    /**
     * @param valorIpiCreditado the valorIpiCreditado to set
     */
    public void setValorIpiCreditado(String valorIpiCreditado) {
        this.valorIpiCreditado = valorIpiCreditado;
    }
    
    

}