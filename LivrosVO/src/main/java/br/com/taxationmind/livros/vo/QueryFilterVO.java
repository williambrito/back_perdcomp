/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class QueryFilterVO {

    private String params;
    private Map<Integer, Object> values;

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public Map<Integer, Object> getValues() {
        if (values == null) {
            values = new HashMap<>();
        }
        return values;
    }

    public void setValues(Map<Integer, Object> values) {
        this.values = values;
    }

}