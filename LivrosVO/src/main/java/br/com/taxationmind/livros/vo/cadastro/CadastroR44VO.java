/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class CadastroR44VO {
    
     @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r44", new FilterVO("r44", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("trimestre",new FilterVO("trimestre",Types.VARCHAR));
        FILTER_MAP.put("ano_calendario", new FilterVO("ano_calendario", Types.VARCHAR));
        FILTER_MAP.put("cnpj_fonte_pagadora", new FilterVO("cnpj_fonte_pagadora", Types.VARCHAR));
        FILTER_MAP.put("mes", new FilterVO("mes", Types.VARCHAR));
        FILTER_MAP.put("valor_retido", new FilterVO("valor_retido", Types.VARCHAR));
        
       
    }
    
    private Long id;
    private String r44;
    private String cnpj_declarante;
    private String cnpj_sucedida;
    private String trimestre;
    private String ano_calendario;
    private String cnpj_fonte_pagadora;
    private String mes;
    private String valor_retido;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r44
     */
    public String getR44() {
        return r44;
    }

    /**
     * @param r44 the r44 to set
     */
    public void setR44(String r44) {
        this.r44 = r44;
    }

    /**
     * @return the cnpj_declarante
     */
    public String getCnpj_declarante() {
        return cnpj_declarante;
    }

    /**
     * @param cnpj_declarante the cnpj_declarante to set
     */
    public void setCnpj_declarante(String cnpj_declarante) {
        this.cnpj_declarante = cnpj_declarante;
    }

    /**
     * @return the cnpj_sucedida
     */
    public String getCnpj_sucedida() {
        return cnpj_sucedida;
    }

    /**
     * @param cnpj_sucedida the cnpj_sucedida to set
     */
    public void setCnpj_sucedida(String cnpj_sucedida) {
        this.cnpj_sucedida = cnpj_sucedida;
    }

    /**
     * @return the trimestre
     */
    public String getTrimestre() {
        return trimestre;
    }

    /**
     * @param trimestre the trimestre to set
     */
    public void setTrimestre(String trimestre) {
        this.trimestre = trimestre;
    }

    /**
     * @return the ano_calendario
     */
    public String getAno_calendario() {
        return ano_calendario;
    }

    /**
     * @param ano_calendario the ano_calendario to set
     */
    public void setAno_calendario(String ano_calendario) {
        this.ano_calendario = ano_calendario;
    }

    /**
     * @return the cnpj_fonte_pagadora
     */
    public String getCnpj_fonte_pagadora() {
        return cnpj_fonte_pagadora;
    }

    /**
     * @param cnpj_fonte_pagadora the cnpj_fonte_pagadora to set
     */
    public void setCnpj_fonte_pagadora(String cnpj_fonte_pagadora) {
        this.cnpj_fonte_pagadora = cnpj_fonte_pagadora;
    }

    /**
     * @return the mes
     */
    public String getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(String mes) {
        this.mes = mes;
    }

    /**
     * @return the valor_retido
     */
    public String getValor_retido() {
        return valor_retido;
    }

    /**
     * @param valor_retido the valor_retido to set
     */
    public void setValor_retido(String valor_retido) {
        this.valor_retido = valor_retido;
    }
    
}