/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class CadastroR29VO {
     @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r29", new FilterVO("r29", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("data_inicial", new FilterVO("data_inicial", Types.VARCHAR));
        FILTER_MAP.put("data_final", new FilterVO("data_final", Types.VARCHAR));
        FILTER_MAP.put("cnpj_fonte_pagadora", new FilterVO("cnpj_fonte_págadora", Types.VARCHAR));
        FILTER_MAP.put("cod_receita", new FilterVO("cod_receita", Types.VARCHAR));
        FILTER_MAP.put("variacao", new FilterVO("variacao", Types.VARCHAR));
        FILTER_MAP.put("retencao_efetuada", new FilterVO("retencao_efetuada", Types.VARCHAR));
        FILTER_MAP.put("valor", new FilterVO("valor", Types.VARCHAR));        

    }

    private Long id;
    private String r29;
    private String cnpjDeclarante;
    private String cnpjSucedida;
    private String dataInicial;
    private String dataFinal;
    private String cnpjFontePagadora;
    private String codigoReceita;
    private String variacao;
    private String retencaoEfetuada;
    private String valor;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r29
     */
    public String getR29() {
        return r29;
    }

    /**
     * @param r29 the r29 to set
     */
    public void setR29(String r29) {
        this.r29 = r29;
    }

    /**
     * @return the cnpjDeclarante
     */
    public String getCnpjDeclarante() {
        return cnpjDeclarante;
    }

    /**
     * @param cnpjDeclarante the cnpjDeclarante to set
     */
    public void setCnpjDeclarante(String cnpjDeclarante) {
        this.cnpjDeclarante = cnpjDeclarante;
    }

    /**
     * @return the cnpjSucedida
     */
    public String getCnpjSucedida() {
        return cnpjSucedida;
    }

    /**
     * @param cnpjSucedida the cnpjSucedida to set
     */
    public void setCnpjSucedida(String cnpjSucedida) {
        this.cnpjSucedida = cnpjSucedida;
    }

    /**
     * @return the dataInicial
     */
    public String getDataInicial() {
        return dataInicial;
    }

    /**
     * @param dataInicial the dataInicial to set
     */
    public void setDataInicial(String dataInicial) {
        this.dataInicial = dataInicial;
    }

    /**
     * @return the dataFinal
     */
    public String getDataFinal() {
        return dataFinal;
    }

    /**
     * @param dataFinal the dataFinal to set
     */
    public void setDataFinal(String dataFinal) {
        this.dataFinal = dataFinal;
    }

    /**
     * @return the cnpjFontePagadora
     */
    public String getCnpjFontePagadora() {
        return cnpjFontePagadora;
    }

    /**
     * @param cnpjFontePagadora the cnpjFontePagadora to set
     */
    public void setCnpjFontePagadora(String cnpjFontePagadora) {
        this.cnpjFontePagadora = cnpjFontePagadora;
    }

    /**
     * @return the codigoReceita
     */
    public String getCodigoReceita() {
        return codigoReceita;
    }

    /**
     * @param codigoReceita the codigoReceita to set
     */
    public void setCodigoReceita(String codigoReceita) {
        this.codigoReceita = codigoReceita;
    }

    /**
     * @return the variacao
     */
    public String getVariacao() {
        return variacao;
    }

    /**
     * @param variacao the variacao to set
     */
    public void setVariacao(String variacao) {
        this.variacao = variacao;
    }

    /**
     * @return the retencaoEfetuada
     */
    public String getRetencaoEfetuada() {
        return retencaoEfetuada;
    }

    /**
     * @param retencaoEfetuada the retencaoEfetuada to set
     */
    public void setRetencaoEfetuada(String retencaoEfetuada) {
        this.retencaoEfetuada = retencaoEfetuada;
    }

    /**
     * @return the valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * @param valor the valor to set
     */
    public void setValor(String valor) {
        this.valor = valor;
    }
    
}
