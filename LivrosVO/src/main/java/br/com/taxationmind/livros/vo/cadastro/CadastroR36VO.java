/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class CadastroR36VO {
    
    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();

    static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r36", new FilterVO("r36", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("data_inicial", new FilterVO("data_inicial", Types.VARCHAR));
        FILTER_MAP.put("data_final", new FilterVO("data_final", Types.VARCHAR));
        FILTER_MAP.put("cnpj_fonte_pagadora", new FilterVO("cnpj_fonte_pagadora", Types.VARCHAR));
        FILTER_MAP.put("cod_receita", new FilterVO("cod_receita", Types.VARCHAR));
        FILTER_MAP.put("variacao", new FilterVO("variacao", Types.VARCHAR));
        FILTER_MAP.put("retencao_efetuada", new FilterVO("retencao_efetuada", Types.VARCHAR));
        FILTER_MAP.put("valor", new FilterVO("valor", Types.VARCHAR));
       
    }
    private Long id;
    private String r36;
    private String cnpj_declarante;
    private String cnpj_sucedida;
    private String data_inicial;
    private String data_final;
    private String cnpj_fonte_pagadora;
    private String cod_receita;
    private String variacao;
    private String retencao_efetuada;
    private String Valor;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r36
     */
    public String getR36() {
        return r36;
    }

    /**
     * @param r36 the r36 to set
     */
    public void setR36(String r36) {
        this.r36 = r36;
    }

    /**
     * @return the cnpj_declarante
     */
    public String getCnpj_declarante() {
        return cnpj_declarante;
    }

    /**
     * @param cnpj_declarante the cnpj_declarante to set
     */
    public void setCnpj_declarante(String cnpj_declarante) {
        this.cnpj_declarante = cnpj_declarante;
    }

    /**
     * @return the cnpj_sucedida
     */
    public String getCnpj_sucedida() {
        return cnpj_sucedida;
    }

    /**
     * @param cnpj_sucedida the cnpj_sucedida to set
     */
    public void setCnpj_sucedida(String cnpj_sucedida) {
        this.cnpj_sucedida = cnpj_sucedida;
    }

    /**
     * @return the data_inicial
     */
    public String getData_inicial() {
        return data_inicial;
    }

    /**
     * @param data_inicial the data_inicial to set
     */
    public void setData_inicial(String data_inicial) {
        this.data_inicial = data_inicial;
    }

    /**
     * @return the data_final
     */
    public String getData_final() {
        return data_final;
    }

    /**
     * @param data_final the data_final to set
     */
    public void setData_final(String data_final) {
        this.data_final = data_final;
    }

    /**
     * @return the cnpj_fonte_pagadora
     */
    public String getCnpj_fonte_pagadora() {
        return cnpj_fonte_pagadora;
    }

    /**
     * @param cnpj_fonte_pagadora the cnpj_fonte_pagadora to set
     */
    public void setCnpj_fonte_pagadora(String cnpj_fonte_pagadora) {
        this.cnpj_fonte_pagadora = cnpj_fonte_pagadora;
    }

    /**
     * @return the cod_receita
     */
    public String getCod_receita() {
        return cod_receita;
    }

    /**
     * @param cod_receita the cod_receita to set
     */
    public void setCod_receita(String cod_receita) {
        this.cod_receita = cod_receita;
    }

    /**
     * @return the variacao
     */
    public String getVariacao() {
        return variacao;
    }

    /**
     * @param variacao the variacao to set
     */
    public void setVariacao(String variacao) {
        this.variacao = variacao;
    }

    /**
     * @return the retencao_efetuada
     */
    public String getRetencao_efetuada() {
        return retencao_efetuada;
    }

    /**
     * @param retencao_efetuada the retencao_efetuada to set
     */
    public void setRetencao_efetuada(String retencao_efetuada) {
        this.retencao_efetuada = retencao_efetuada;
    }

    /**
     * @return the Valor
     */
    public String getValor() {
        return Valor;
    }

    /**
     * @param Valor the Valor to set
     */
    public void setValor(String Valor) {
        this.Valor = Valor;
    }

    
}