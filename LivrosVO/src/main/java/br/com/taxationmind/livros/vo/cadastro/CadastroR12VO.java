/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.livros.vo.cadastro;

import br.com.taxationmind.livros.vo.FilterVO;
import com.fasterxml.jackson.annotation.JsonIgnore;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class CadastroR12VO {
    
    @JsonIgnore
    public static final Map<String, FilterVO> FILTER_MAP = new HashMap<>();
    
     static {
        FILTER_MAP.put("id", new FilterVO("id", Types.BIGINT));
        FILTER_MAP.put("r12", new FilterVO("r11", Types.VARCHAR));
        FILTER_MAP.put("cnpj_declarante", new FilterVO("cnpj_declarante", Types.VARCHAR));
        FILTER_MAP.put("cnpj_sucedida", new FilterVO("cnpj_sucedida", Types.VARCHAR));
        FILTER_MAP.put("cnpj_detentor", new FilterVO("cnpj_detentor", Types.VARCHAR));
        FILTER_MAP.put("ano_apuracao", new FilterVO("ano_apuracao", Types.VARCHAR));
        FILTER_MAP.put("mes_apuracao", new FilterVO("mes_apuracao", Types.VARCHAR));
        FILTER_MAP.put("decendio", new FilterVO("decendio", Types.VARCHAR));
        FILTER_MAP.put("cfop", new FilterVO("cfop", Types.VARCHAR));
        FILTER_MAP.put("base_calculo", new FilterVO("base_calculo", Types.VARCHAR));
        FILTER_MAP.put("ipi_debitado", new FilterVO("ipi_debitado", Types.VARCHAR));
        FILTER_MAP.put("isentas_nao_tributadas", new FilterVO("isentas_nao_tributadas", Types.VARCHAR));
        FILTER_MAP.put("outras", new FilterVO("outras", Types.VARCHAR));
        

    }

    private Long id;
    private String r12;
    private String cnpjDeclarante;
    private String cnpjSucedida;
    private String cnpjDetentor;
    private String anoApuracao;
    private String mesApuracao;
    private String decendio;
    private String cfop;
    private String baseCalculo;
    private String ipiDebitado;
    private String isentasNaotributada;
    private String outras;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the r12
     */
    public String getR12() {
        return r12;
    }

    /**
     * @param r12 the r12 to set
     */
    public void setR12(String r12) {
        this.r12 = r12;
    }

    /**
     * @return the cnpjDeclarante
     */
    public String getCnpjDeclarante() {
        return cnpjDeclarante;
    }

    /**
     * @param cnpjDeclarante the cnpjDeclarante to set
     */
    public void setCnpjDeclarante(String cnpjDeclarante) {
        this.cnpjDeclarante = cnpjDeclarante;
    }

    /**
     * @return the cnpjSucedida
     */
    public String getCnpjSucedida() {
        return cnpjSucedida;
    }

    /**
     * @param cnpjSucedida the cnpjSucedida to set
     */
    public void setCnpjSucedida(String cnpjSucedida) {
        this.cnpjSucedida = cnpjSucedida;
    }

    /**
     * @return the cnpjDetentor
     */
    public String getCnpjDetentor() {
        return cnpjDetentor;
    }

    /**
     * @param cnpjDetentor the cnpjDetentor to set
     */
    public void setCnpjDetentor(String cnpjDetentor) {
        this.cnpjDetentor = cnpjDetentor;
    }

    /**
     * @return the anoApuracao
     */
    public String getAnoApuracao() {
        return anoApuracao;
    }

    /**
     * @param anoApuracao the anoApuracao to set
     */
    public void setAnoApuracao(String anoApuracao) {
        this.anoApuracao = anoApuracao;
    }

    /**
     * @return the mesApuracao
     */
    public String getMesApuracao() {
        return mesApuracao;
    }

    /**
     * @param mesApuracao the mesApuracao to set
     */
    public void setMesApuracao(String mesApuracao) {
        this.mesApuracao = mesApuracao;
    }

    /**
     * @return the decendio
     */
    public String getDecendio() {
        return decendio;
    }

    /**
     * @param decendio the decendio to set
     */
    public void setDecendio(String decendio) {
        this.decendio = decendio;
    }

    /**
     * @return the cfop
     */
    public String getCfop() {
        return cfop;
    }

    /**
     * @param cfop the cfop to set
     */
    public void setCfop(String cfop) {
        this.cfop = cfop;
    }

    /**
     * @return the baseCalculo
     */
    public String getBaseCalculo() {
        return baseCalculo;
    }

    /**
     * @param baseCalculo the baseCalculo to set
     */
    public void setBaseCalculo(String baseCalculo) {
        this.baseCalculo = baseCalculo;
    }

    /**
     * @return the ipiDebitado
     */
    public String getIpiDebitado() {
        return ipiDebitado;
    }

    /**
     * @param ipiDebitado the ipiDebitado to set
     */
    public void setIpiDebitado(String ipiDebitado) {
        this.ipiDebitado = ipiDebitado;
    }

    /**
     * @return the isentasNaotributada
     */
    public String getIsentasNaotributada() {
        return isentasNaotributada;
    }

    /**
     * @param isentasNaotributada the isentasNaotributada to set
     */
    public void setIsentasNaotributada(String isentasNaotributada) {
        this.isentasNaotributada = isentasNaotributada;
    }

    /**
     * @return the outras
     */
    public String getOutras() {
        return outras;
    }

    /**
     * @param outras the outras to set
     */
    public void setOutras(String outras) {
        this.outras = outras;
    }
    
    
}
