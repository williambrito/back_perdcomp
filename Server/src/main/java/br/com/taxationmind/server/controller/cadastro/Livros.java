/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.controller.cadastro;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.database.dao.LivrosDAO;
import br.com.taxationmind.database.dao.utils.DAOErrorConverter;
import br.com.taxationmind.database.dao.utils.Utils;
import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.RetornoVO;
import br.com.taxationmind.livros.vo.cadastro.LivrosVO;
import br.com.taxationmind.server.controller.Controller;
import com.fasterxml.jackson.core.type.TypeReference;
import io.javalin.http.Context;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author William Brito
 */
public class Livros extends Controller<LivrosVO> {
    
    private static final LivrosDAO Livros_DAO = new LivrosDAO();

     public Livros(ConnectionFactoryPool conn, String schema) {
        this.conn = conn;
        this.schema = schema;
    }

    @Override
    public void getAll(Context ctx) {
        if (ctx.queryParamMap().isEmpty()) {
            try {
                ctx.json(Livros_DAO.getAll(conn, schema, null));
            } catch (SQLException ex) {
                Logger.getLogger(Livros.class.getName()).log(Level.SEVERE, "Erro ao recuperar lista de ticket", ex);
                ctx.json(new RetornoVO(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
            }
        } else {
            get(ctx);
        }
    }

    @Override
    public void get(Context ctx) {

        try {
            ctx.json(Livros_DAO.get(conn, schema, Utils.paramConverter(LivrosVO.FILTER_MAP, ctx.queryParamMap())));
        } catch (SQLException ex) {
            Logger.getLogger(Livros.class.getName()).log(Level.SEVERE, "Erro ao procurar Status do ticket", ex);
            ctx.json(new RetornoVO<>(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
        }
    }

    @Override
    public void insert(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<LivrosVO> listStatus = this.processJson(ctx, new TypeReference<List<LivrosVO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (Livros_DAO.insert(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(INSERT, true, "Dados [" + dados.getBook_name()+ "] inseridos com sucesso", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Livros.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    listRetorno.add(new RetornoVO(INSERT, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(Livros.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void update(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<LivrosVO> listProd = this.processJson(ctx, new TypeReference<List<LivrosVO>>() {
        });
        if (listProd != null) {
            listProd.forEach(dados -> {
                try {
                    if (Livros_DAO.update(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(UPDATE, true, "Cadastro status [" + dados.getBook_name()+ "] atualizado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(UPDATE, false, null, "Cadastro status [" + dados.getId() + "] sem correspondente para atualização", dados));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Livros.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    listRetorno.add(new RetornoVO(UPDATE, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(Livros.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void delete(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(DELETE, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<LivrosVO> listStatus = this.processJson(ctx, new TypeReference<List<LivrosVO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (Livros_DAO.delete(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(DELETE, true, "Dados Id [" + dados.getId() + "] deletado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(DELETE, false, "Dados Id [" + dados.getId() + "] sem correspondente para remoção", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(Livros.class.getName()).log(Level.SEVERE, "Erro ao remover Dados", ex);
                    listRetorno.add(new RetornoVO(DELETE, false, null, DAOErrorConverter.convertSQLException(ex)));
                }
            });
        }
        ctx.json(listRetorno);

    }    
    
}
