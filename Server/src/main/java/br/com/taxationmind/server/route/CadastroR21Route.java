/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR21;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR21Route extends Route {

    public CadastroR21Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR21Route();
    }

    private void createCadastroR21Route() {

        final CadastroR21 cadastroR21 = new CadastroR21(connectionFactory, DEFAULT_SCHEMA);
        final String R21_PATH = defaultPrefixPath("r21");
        final String R21_TXT_PATH = R21_PATH + "/txt";

        //app.get(R21_PATH, ctx -> cadastroR21.getAll(ctx));
        app.get(R21_TXT_PATH, ctx -> cadastroR21.creatingFile(ctx));
        app.get(R21_PATH, ctx -> cadastroR21.get(ctx));
        app.post(R21_PATH, ctx -> cadastroR21.insert(ctx));
        app.patch(R21_PATH, ctx -> cadastroR21.update(ctx));
        app.delete(R21_PATH, ctx -> cadastroR21.delete(ctx));
    }

}
