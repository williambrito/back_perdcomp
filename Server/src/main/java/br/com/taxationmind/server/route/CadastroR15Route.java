/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR15;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR15Route extends Route {

    public CadastroR15Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR15Route();
    }

    private void createCadastroR15Route() {

        final CadastroR15 cadastroR15 = new CadastroR15(connectionFactory, DEFAULT_SCHEMA);
        final String R15_PATH = defaultPrefixPath("r15");
         final String R15_TXT_PATH = R15_PATH + "/txt";
        

        //app.get(R15_PATH, ctx -> cadastroR15.getAll(ctx));
        app.get(R15_TXT_PATH, ctx -> cadastroR15.creatingFile(ctx));
        app.get(R15_PATH, ctx -> cadastroR15.get(ctx));
        app.post(R15_PATH, ctx -> cadastroR15.insert(ctx));
        app.patch(R15_PATH, ctx -> cadastroR15.update(ctx));
        app.delete(R15_PATH, ctx -> cadastroR15.delete(ctx));
    }

}
