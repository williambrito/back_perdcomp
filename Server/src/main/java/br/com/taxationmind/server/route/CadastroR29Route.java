/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR29;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR29Route extends Route {

    public CadastroR29Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR29Route();
    }

    private void createCadastroR29Route() {

        final CadastroR29 cadastroR29 = new CadastroR29(connectionFactory, DEFAULT_SCHEMA);
        final String R29_PATH = defaultPrefixPath("r29");
         final String R29_TXT_PATH = R29_PATH + "/txt";
        

        //app.get(R29_PATH, ctx -> cadastroR29.getAll(ctx));
        app.get(R29_TXT_PATH, ctx -> cadastroR29.creatingFile(ctx));
        app.get(R29_PATH, ctx -> cadastroR29.get(ctx));
        app.post(R29_PATH, ctx -> cadastroR29.insert(ctx));
        app.patch(R29_PATH, ctx -> cadastroR29.update(ctx));
        app.delete(R29_PATH, ctx -> cadastroR29.delete(ctx));
    }

}
