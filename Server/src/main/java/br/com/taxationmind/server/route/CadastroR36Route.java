/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR36;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR36Route extends Route {

    public CadastroR36Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR36Route();
    }

    private void createCadastroR36Route() {

        final CadastroR36 cadastroR36 = new CadastroR36(connectionFactory, DEFAULT_SCHEMA);
        final String R36_PATH = defaultPrefixPath("r36");
        final String R36_TXT_PATH = R36_PATH + "/txt";
        

        //app.get(R36_PATH, ctx -> cadastroR36.getAll(ctx));
        app.get(R36_TXT_PATH, ctx -> cadastroR36.creatingFile(ctx));
        app.get(R36_PATH, ctx -> cadastroR36.get(ctx));
        app.post(R36_PATH, ctx -> cadastroR36.insert(ctx));
        app.patch(R36_PATH, ctx -> cadastroR36.update(ctx));
        app.delete(R36_PATH, ctx -> cadastroR36.delete(ctx));
    }

}
