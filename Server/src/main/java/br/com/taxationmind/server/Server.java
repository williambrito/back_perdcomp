/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.fileutils.FileProperties;
import br.com.taxationmind.server.route.BookDataRoute;
import br.com.taxationmind.server.route.CadastroR11Route;
import br.com.taxationmind.server.route.CadastroR12Route;
import br.com.taxationmind.server.route.CadastroR13Route;
import br.com.taxationmind.server.route.CadastroR15Route;
import br.com.taxationmind.server.route.CadastroR21Route;
import br.com.taxationmind.server.route.CadastroR29Route;
import br.com.taxationmind.server.route.CadastroR36Route;
import br.com.taxationmind.server.route.CadastroR42Route;
import br.com.taxationmind.server.route.CadastroR44Route;
import io.javalin.Javalin;
import java.io.File;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.session.DefaultSessionCache;
import org.eclipse.jetty.server.session.FileSessionDataStore;
import org.eclipse.jetty.server.session.SessionCache;
import org.eclipse.jetty.server.session.SessionHandler;

/**
 *
 * @author William Brito
 */
public class Server {

    private int httpPort = 7010;
    private int httpsPort = 7443;
    private String host = "0.0.0.0";
    private String schema = "perdcomp_homolog";
    private final FileProperties impp = FileProperties.getInstance();
    private final ConnectionFactoryPool connectionFactory;

    public Server() {
        connectionFactory = ConnectionFactoryPool.getInstance(impp.getProperty("DB_URL"), impp.getProperty("DB_USER"), impp.getProperty("DB_PASSWD"));
        if (impp.getMap().containsKey("HOST")) {
            host = impp.getProperty("HOST");
        }

        if (impp.getMap().containsKey("HTTP_PORT")) {
            httpPort = Integer.parseInt(impp.getProperty("HTTP_PORT"));
        }

        if (impp.getMap().containsKey("HTTPS_PORT")) {
            httpsPort = Integer.parseInt(impp.getProperty("HTTPS_PORT"));
        }
    }

    public void startRestServer() {
        Javalin app = Javalin.create(config -> {
            config.jetty.sessionHandler(() -> fileSessionHandler());
            config.jetty.server(() -> {
                org.eclipse.jetty.server.Server server = new org.eclipse.jetty.server.Server();
                ServerConnector sslConnector = new ServerConnector(server);
                sslConnector.setHost(host);
                sslConnector.setPort(httpsPort);
                ServerConnector connector = new ServerConnector(server);
                connector.setHost(host);
                connector.setPort(httpPort);
                server.setConnectors(new Connector[]{sslConnector, connector});

                return server;
            });
        }).start();

        initRoutes(app);
    }

    private void initRoutes(Javalin app) {
        BookDataRoute bookdataRoute = new BookDataRoute(app, connectionFactory, schema);
        CadastroR12Route cadastroR12Route = new CadastroR12Route(app, connectionFactory, schema);
        CadastroR13Route cadastroR13Route = new CadastroR13Route(app, connectionFactory, schema);
        CadastroR15Route cadastroR15Route = new CadastroR15Route(app, connectionFactory, schema);
        CadastroR21Route cadastroR21Route = new CadastroR21Route(app, connectionFactory, schema);
        CadastroR29Route cadastroR29Route = new CadastroR29Route(app, connectionFactory, schema);
        CadastroR36Route cadastroR36Route = new CadastroR36Route(app, connectionFactory, schema);
        CadastroR42Route cadastroR42Route = new CadastroR42Route(app, connectionFactory, schema);
        CadastroR44Route cadastroR44Route = new CadastroR44Route(app, connectionFactory, schema);

    }

    private SessionHandler fileSessionHandler() {
        final SessionHandler sessionHandler = new SessionHandler();
        SessionCache sessionCache = new DefaultSessionCache(sessionHandler);
        sessionCache.setSessionDataStore(fileSessionDataStore());
        sessionHandler.setSessionCache(sessionCache);
        sessionHandler.setHttpOnly(true);
        sessionHandler.getSessionCookieConfig().setHttpOnly(true);
        sessionHandler.getSessionCookieConfig().setMaxAge(60);
        sessionHandler.getSessionCookieConfig().setComment("__SAME_SITE_STRICT__");
        //sessionHandler.setMaxInactiveInterval(600);

        return sessionHandler;
    }

    private FileSessionDataStore fileSessionDataStore() {
        FileSessionDataStore fileSessionDataStore = new FileSessionDataStore();
        File baseDir = new File(System.getProperty("java.io.tmpdir"));
        File storeDir = new File(baseDir, "javalin-session-store");
        storeDir.mkdir();
        fileSessionDataStore.setStoreDir(storeDir);
        return fileSessionDataStore;
    }

}
