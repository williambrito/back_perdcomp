/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR44;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR44Route extends Route {

    public CadastroR44Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR44Route();
    }

    private void createCadastroR44Route() {

        final CadastroR44 cadastroR44 = new CadastroR44(connectionFactory, DEFAULT_SCHEMA);
        final String R44_PATH = defaultPrefixPath("r44");
        final String R44_TXT_PATH = R44_PATH + "/txt";

        //app.get(R44_PATH, ctx -> cadastroR44.getAll(ctx));
        app.get(R44_TXT_PATH, ctx -> cadastroR44.creatingFile(ctx));
        app.get(R44_PATH, ctx -> cadastroR44.get(ctx));
        app.post(R44_PATH, ctx -> cadastroR44.insert(ctx));
        app.patch(R44_PATH, ctx -> cadastroR44.update(ctx));
        app.delete(R44_PATH, ctx -> cadastroR44.delete(ctx));
    }

}
