/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class Route {

    public static final String MIME_JSON = "application/json";
    private static final String PREFIX_SECURED_PATH = "api/v1.0/";
    protected final Javalin app;
    protected static final String DEFAULT_SCHEMA = "perdcomp_homolog";
    protected final ConnectionFactoryPool connectionFactory;
    protected String userSchema;

    public Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        this.app = app;
        this.connectionFactory = connectionFactory;
        this.userSchema = userSchema;
    }

    public static String defaultPrefixPath(String path) {
        return PREFIX_SECURED_PATH + path;
    }

}
