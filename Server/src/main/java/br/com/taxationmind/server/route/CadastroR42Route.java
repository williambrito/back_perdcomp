/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR42;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR42Route extends Route {

    public CadastroR42Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR42Route();
        
    }

    private void createCadastroR42Route() {

        final CadastroR42 cadastroR42 = new CadastroR42(connectionFactory, DEFAULT_SCHEMA);
        final String R42_PATH = defaultPrefixPath("r42");
        final String R42_TXT_PATH = R42_PATH + "/txt";

        //app.get(R42_PATH, ctx -> cadastroR42.getAll(ctx));
        app.get(R42_TXT_PATH, ctx -> cadastroR42.creatingFile(ctx));
        app.get(R42_PATH, ctx -> cadastroR42.get(ctx));
        app.post(R42_PATH, ctx -> cadastroR42.insert(ctx));
        app.patch(R42_PATH, ctx -> cadastroR42.update(ctx));
        app.delete(R42_PATH, ctx -> cadastroR42.delete(ctx));
    }

}
