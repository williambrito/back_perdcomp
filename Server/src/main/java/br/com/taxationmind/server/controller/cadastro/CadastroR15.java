/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.controller.cadastro;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.database.dao.R15DAO;
import br.com.taxationmind.database.dao.utils.DAOErrorConverter;
import br.com.taxationmind.database.dao.utils.Utils;
import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.RetornoVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR15VO;
import br.com.taxationmind.server.controller.Controller;
import com.fasterxml.jackson.core.type.TypeReference;
import io.javalin.http.Context;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author William Brito
 */
public class CadastroR15 extends Controller<CadastroR15VO> {
    
    private static final R15DAO R15_DAO = new R15DAO();

     public CadastroR15(ConnectionFactoryPool conn, String schema) {
        this.conn = conn;
        this.schema = schema;
    }

    @Override
    public void getAll(Context ctx) {
        if (ctx.queryParamMap().isEmpty()) {
            try {
                ctx.json(R15_DAO.getAll(conn, schema, null));
            } catch (SQLException ex) {
                Logger.getLogger(CadastroR15.class.getName()).log(Level.SEVERE, "Erro ao recuperar lista de ticket", ex);
                ctx.json(new RetornoVO(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
            }
        } else {
            get(ctx);
        }
    }

    @Override
    public void get(Context ctx) {

        try {
            ctx.json(R15_DAO.get(conn, schema, Utils.paramConverter(CadastroR15VO.FILTER_MAP, ctx.queryParamMap())));
        } catch (SQLException ex) {
            Logger.getLogger(CadastroR15.class.getName()).log(Level.SEVERE, "Erro ao procurar Status do ticket", ex);
            ctx.json(new RetornoVO<>(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
        }
    }

    @Override
    public void insert(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR15VO> listStatus = this.processJson(ctx, new TypeReference<List<CadastroR15VO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (R15_DAO.insert(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(INSERT, true, "Dados [" + dados.getR15() + "] inseridos com sucesso", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR15.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    listRetorno.add(new RetornoVO(INSERT, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(CadastroR15.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void update(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR15VO> listProd = this.processJson(ctx, new TypeReference<List<CadastroR15VO>>() {
        });
        if (listProd != null) {
            listProd.forEach(dados -> {
                try {
                    if (R15_DAO.update(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(UPDATE, true, "Cadastro status [" + dados.getR15() + "] atualizado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(UPDATE, false, null, "Cadastro status [" + dados.getId() + "] sem correspondente para atualização", dados));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR15.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    listRetorno.add(new RetornoVO(UPDATE, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(CadastroR15.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void delete(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(DELETE, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR15VO> listStatus = this.processJson(ctx, new TypeReference<List<CadastroR15VO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (R15_DAO.delete(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(DELETE, true, "Dados Id [" + dados.getId() + "] deletado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(DELETE, false, "Dados Id [" + dados.getId() + "] sem correspondente para remoção", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR15.class.getName()).log(Level.SEVERE, "Erro ao remover Dados", ex);
                    listRetorno.add(new RetornoVO(DELETE, false, null, DAOErrorConverter.convertSQLException(ex)));
                }
            });
        }
        ctx.json(listRetorno);

    }
    public void creatingFile(Context ctx) throws UnsupportedEncodingException, IOException {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
        }

        if (ctx.body().contains("relatorios")) {
            String fileName = "relatorio.txt";
            FileWriter fileWriter = new FileWriter(fileName);
            InputStream inputStream = new FileInputStream(fileName);
            OutputStream outputStream = ctx.outputStream();

            byte[] request = ctx.bodyAsBytes();
            String jsonString = new String(request, "UTF-8");
            JSONObject json = new JSONObject(jsonString);
            JSONArray arrayDosIds = json.getJSONArray("relatorios");
            
            for (int i = 0; i < arrayDosIds.toList().size(); i++) {
                try {                    
                    JSONObject object = arrayDosIds.getJSONObject(i);                    
                    int id = object.getInt("id");

                    FilterVO filter = new FilterVO("id", Types.BIGINT, String.valueOf(id));
                    List<CadastroR15VO> responseRelatorio = R15_DAO.get(conn, schema, Arrays.asList(filter));

                    CadastroR15VO relatorio = responseRelatorio.get(0);
                    fileWriter.write(relatorio.getR15()== null ? "R15" : relatorio.getR15());                    
                    fileWriter.write(relatorio.getCnpj_declarante()== null ? "00000000000000" : relatorio.getCnpj_declarante());
                    fileWriter.write(relatorio.getCnpj_sucedida()== null ? "              " : relatorio.getCnpj_sucedida());
                    fileWriter.write(relatorio.getCnpj_emitente()== null ? "00000000000000" : relatorio.getCnpj_emitente());
                    fileWriter.write(relatorio.getNum_nota()== null ? "00000000000000" : relatorio.getNum_nota());
                    fileWriter.write(relatorio.getSerie()== null ? "0000" : relatorio.getSerie());
                    fileWriter.write(relatorio.getData_emissao()== null ? "00" : relatorio.getData_emissao());
                    fileWriter.write(relatorio.getData_entrada()== null ? "1" : relatorio.getData_entrada());
                    fileWriter.write(relatorio.getCfop()== null ? "00000000000000" : relatorio.getCfop());
                    fileWriter.write(relatorio.getValor_ipi_destacado()== null ? "0000000000000" : relatorio.getValor_ipi_destacado());
                    fileWriter.write(relatorio.getValor_ipi_creditado()== null ? "00000000000000" : relatorio.getValor_ipi_creditado());
                    fileWriter.write(relatorio.getEspecie_cred()== null ? "00000000000000" : relatorio.getEspecie_cred());
                    fileWriter.write(relatorio.getDecendio()== null ? "00000000000000" : relatorio.getDecendio());
                    fileWriter.write(relatorio.getMes_periodo()== null ? "00000000000000" : relatorio.getMes_periodo());
                    fileWriter.write(relatorio.getAno_periodo()== null ? "00000000000000" : relatorio.getAno_periodo());
                   
                    
                    
                    
                    
                    fileWriter.write("\n");

                } catch (IOException ex) {
                    Logger.getLogger(CadastroR11.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR11.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            fileWriter.close();

            ctx.contentType("text/plain");
            ctx.header("Content-Disposition", "attachment; filename=" + fileName);

            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();

        }
    }

}



