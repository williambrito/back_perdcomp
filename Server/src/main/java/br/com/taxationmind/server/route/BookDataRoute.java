/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.BookData;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class BookDataRoute extends Route {
    
    public BookDataRoute(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createBookDataRoute();
    }

    private void createBookDataRoute() {

        final BookData bookdata = new BookData(connectionFactory, DEFAULT_SCHEMA);
        final String BOOKDATA_PATH = defaultPrefixPath("r11");
        //final String BOOKDATA_TXT_PATH = BOOKDATA_PATH + "/txt";

        //app.get(R11_PATH, ctx -> cadastroR11.getAll(ctx));
        //app.get(BOOKDATA_TXT_PATH, ctx -> bookdata.creatingFile(ctx));
        app.get(BOOKDATA_PATH, ctx -> bookdata.get(ctx));
        app.post(BOOKDATA_PATH, ctx -> bookdata.insert(ctx));
        app.patch(BOOKDATA_PATH, ctx -> bookdata.update(ctx));
        app.delete(BOOKDATA_PATH, ctx -> bookdata.delete(ctx));
    }

}

    

