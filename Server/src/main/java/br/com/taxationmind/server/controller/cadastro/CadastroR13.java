/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.controller.cadastro;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.database.dao.R13DAO;
import br.com.taxationmind.database.dao.utils.DAOErrorConverter;
import br.com.taxationmind.database.dao.utils.Utils;
import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.RetornoVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR13VO;
import br.com.taxationmind.server.controller.Controller;
import com.fasterxml.jackson.core.type.TypeReference;
import io.javalin.http.Context;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author William Brito
 */
public class CadastroR13 extends Controller<CadastroR13VO> {
    
    private static final R13DAO R13_DAO = new R13DAO();

     public CadastroR13(ConnectionFactoryPool conn, String schema) {
        this.conn = conn;
        this.schema = schema;
    }

    @Override
    public void getAll(Context ctx) {
        if (ctx.queryParamMap().isEmpty()) {
            try {
                ctx.json(R13_DAO.getAll(conn, schema, null));
            } catch (SQLException ex) {
                Logger.getLogger(CadastroR13.class.getName()).log(Level.SEVERE, "Erro ao recuperar lista de ticket", ex);
                ctx.json(new RetornoVO(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
            }
        } else {
            get(ctx);
        }
    }

    @Override
    public void get(Context ctx) {

        try {
            ctx.json(R13_DAO.get(conn, schema, Utils.paramConverter(CadastroR13VO.FILTER_MAP, ctx.queryParamMap())));
        } catch (SQLException ex) {
            Logger.getLogger(CadastroR13.class.getName()).log(Level.SEVERE, "Erro ao procurar Status do ticket", ex);
            ctx.json(new RetornoVO<>(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
        }
    }

    @Override
    public void insert(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR13VO> listStatus = this.processJson(ctx, new TypeReference<List<CadastroR13VO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (R13_DAO.insert(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(INSERT, true, "Dados [" + dados.getR13() + "] inseridos com sucesso", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR13.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    listRetorno.add(new RetornoVO(INSERT, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(CadastroR13.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void update(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR13VO> listProd = this.processJson(ctx, new TypeReference<List<CadastroR13VO>>() {
        });
        if (listProd != null) {
            listProd.forEach(dados -> {
                try {
                    if (R13_DAO.update(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(UPDATE, true, "Cadastro status [" + dados.getId() + "] atualizado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(UPDATE, false, null, "Cadastro status [" + dados.getId() + "] sem correspondente para atualização", dados));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR13.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    listRetorno.add(new RetornoVO(UPDATE, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(CadastroR13.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void delete(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(DELETE, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR13VO> listStatus = this.processJson(ctx, new TypeReference<List<CadastroR13VO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (R13_DAO.delete(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(DELETE, true, "Dados Id [" + dados.getId() + "] deletado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(DELETE, false, "Dados Id [" + dados.getId() + "] sem correspondente para remoção", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR13.class.getName()).log(Level.SEVERE, "Erro ao remover Dados", ex);
                    listRetorno.add(new RetornoVO(DELETE, false, null, DAOErrorConverter.convertSQLException(ex)));
                }
            });
        }
        ctx.json(listRetorno);

    }
    public void creatingFile(Context ctx) throws UnsupportedEncodingException, IOException {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
        }

        if (ctx.body().contains("relatorios")) {
            String fileName = "relatorio.txt";
            FileWriter fileWriter = new FileWriter(fileName);
            InputStream inputStream = new FileInputStream(fileName);
            OutputStream outputStream = ctx.outputStream();

            byte[] request = ctx.bodyAsBytes();
            String jsonString = new String(request, "UTF-8");
            JSONObject json = new JSONObject(jsonString);
            JSONArray arrayDosIds = json.getJSONArray("relatorios");
            
            for (int i = 0; i < arrayDosIds.toList().size(); i++) {
                try {                    
                    JSONObject object = arrayDosIds.getJSONObject(i);                    
                    int id = object.getInt("id");

                    FilterVO filter = new FilterVO("id", Types.BIGINT, String.valueOf(id));
                    List<CadastroR13VO> responseRelatorio = R13_DAO.get(conn, schema, Arrays.asList(filter));

                    CadastroR13VO relatorio = responseRelatorio.get(0);
                    fileWriter.write(relatorio.getR13()== null ? "R13" : relatorio.getR13());                    
                    fileWriter.write(relatorio.getCnpjDeclarante()== null ? "00000000000000" : relatorio.getCnpjDeclarante());
                    fileWriter.write(relatorio.getCnpjSucedida()== null ? "              " : relatorio.getCnpjSucedida());
                    fileWriter.write(relatorio.getCnpjDetentor()== null ? "00000000000000" : relatorio.getCnpjDetentor());
                    fileWriter.write(relatorio.getNumNota()== null ? "0000" : relatorio.getNumNota());
                    fileWriter.write(relatorio.getSerie()== null ? "00" : relatorio.getSerie());
                    fileWriter.write(relatorio.getDataEmissao()== null ? "1" : relatorio.getDataEmissao());
                    fileWriter.write(relatorio.getDataEntrada()== null ? "00000000000000" : relatorio.getDataEntrada());
                    fileWriter.write(relatorio.getCfop()== null ? "0000000000000" : relatorio.getCfop());
                    fileWriter.write(relatorio.getValorTotal()== null ? "00000000000000" : relatorio.getValorTotal());
                    fileWriter.write(relatorio.getValorIpiDestacado()== null ? "00000000000000" : relatorio.getValorIpiDestacado());
                    fileWriter.write(relatorio.getValorIpiCreditado()== null ? "00000000000000" : relatorio.getValorIpiCreditado());
                    
                    
                    fileWriter.write("\n");

                } catch (IOException ex) {
                    Logger.getLogger(CadastroR12.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR12.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            fileWriter.close();

            ctx.contentType("text/plain");
            ctx.header("Content-Disposition", "attachment; filename=" + fileName);

            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();

        }
    }

}


