/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.controller;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.livros.vo.RetornoVO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.UnrecognizedPropertyException;
import io.javalin.http.Context;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author William Brito
 * @param<T>
 */
public abstract class Controller<T> {

    protected ConnectionFactoryPool conn;
    protected String schema = "perdcomp_homolog";
    protected long userId;
    protected String errorMsg;
    protected List<RetornoVO> listRetorno = new ArrayList<>();
    private final ObjectMapper objectMapper = new ObjectMapper().configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    protected static final String JSON = "json";
    protected static final String SEARCH = "search";
    protected static final String INSERT = "insert";
    protected static final String UPDATE = "update";
    protected static final String DELETE = "delete";
    protected static final String CANCEL = "cancel";
    protected static final String FILA = "fila";
    protected static final String ENVIADO = "enviado";
    protected static final String REPORT = "relatorio";
    protected static final String EXPORT = "export";
    protected static final String UPLOAD = "upload";

    public abstract void insert(Context ctx);

    public abstract void update(Context ctx);

    public abstract void delete(Context ctx);

    public abstract void getAll(Context ctx);

    public abstract void get(Context ctx);

    public <T> T processJson(final Context ctx, TypeReference<T> typeRef) {
        String bodyJson = ctx.body();
        if (!bodyJson.trim().startsWith("[")) {
            bodyJson = "[" + bodyJson + "]";
        }

        return processJson(bodyJson, typeRef);
    }

    public <T> T processJson(final String bodyJson, TypeReference<T> typeRef) {
        T t = null;

        try {
            t = objectMapper.readValue(bodyJson, typeRef);
        } catch (UnrecognizedPropertyException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, "Propriedade não reconhecida", ex);
            listRetorno.add(new RetornoVO(JSON, false, null, "Propriedade [" + ex.getPropertyName() + "] não exite. Segue lista de propriedades válidas " + ex.getKnownPropertyIds()));
        } catch (JsonProcessingException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, "Erro ao processar JSON", ex);
            listRetorno.add(new RetornoVO(JSON, false, null, ex.getMessage()));
        }

        return t;
    }
}
