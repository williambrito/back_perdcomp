/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.controller.cadastro;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.database.dao.R29DAO;
import br.com.taxationmind.database.dao.utils.DAOErrorConverter;
import br.com.taxationmind.database.dao.utils.Utils;
import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.RetornoVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR29VO;
import br.com.taxationmind.server.controller.Controller;
import com.fasterxml.jackson.core.type.TypeReference;
import io.javalin.http.Context;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;







/**
 *
 * @author William Brito
 */
public class CadastroR29 extends Controller<CadastroR29VO> {
    
 private static final R29DAO R29_DAO = new R29DAO();

     public CadastroR29(ConnectionFactoryPool conn, String schema) {
        this.conn = conn;
        this.schema = schema;
    }

    @Override
    public void getAll(Context ctx) {
        if (ctx.queryParamMap().isEmpty()) {
            try {
                ctx.json(R29_DAO.getAll(conn, schema, null));
            } catch (SQLException ex) {
                Logger.getLogger(CadastroR29.class.getName()).log(Level.SEVERE, "Erro ao recuperar lista de ticket", ex);
                ctx.json(new RetornoVO(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
            }
        } else {
            get(ctx);
        }
    }

    @Override
    public void get(Context ctx) {

        try {
            ctx.json(R29_DAO.get(conn, schema, Utils.paramConverter(CadastroR29VO.FILTER_MAP, ctx.queryParamMap())));
        } catch (SQLException ex) {
            Logger.getLogger(CadastroR29.class.getName()).log(Level.SEVERE, "Erro ao procurar Status do ticket", ex);
            ctx.json(new RetornoVO<>(SEARCH, false, null, DAOErrorConverter.convertSQLException(ex)));
        }
    }

    @Override
    public void insert(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR29VO> listStatus = this.processJson(ctx, new TypeReference<List<CadastroR29VO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (R29_DAO.insert(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(INSERT, true, "Dados [" + dados.getR29() + "] inseridos com sucesso", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR29.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    listRetorno.add(new RetornoVO(INSERT, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(CadastroR29.class.getName()).log(Level.SEVERE, "Erro ao inserir dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void update(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR29VO> listProd = this.processJson(ctx, new TypeReference<List<CadastroR29VO>>() {
        });
        if (listProd != null) {
            listProd.forEach(dados -> {
                try {
                    if (R29_DAO.update(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(UPDATE, true, "Cadastro status [" + dados.getR29()+ "] atualizado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(UPDATE, false, null, "Cadastro status [" + dados.getId() + "] sem correspondente para atualização", dados));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR29.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    listRetorno.add(new RetornoVO(UPDATE, false, null, DAOErrorConverter.convertSQLException(ex), dados));
                } catch (io.javalin.http.BadRequestResponse ex) {
                    Logger.getLogger(CadastroR29.class.getName()).log(Level.SEVERE, "Erro ao atualizar dados", ex);
                    errorMsg = ex.getMessage();
                    if (ex.getMessage().contains("Couldn't deserialize body to")) {
                        errorMsg = "Json incompatível";
                    }
                    listRetorno.add(new RetornoVO(UPDATE, false, null, errorMsg, dados));
                }
            });
        }

        ctx.json(listRetorno);

    }

    @Override
    public void delete(Context ctx) {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(DELETE, false, null, "Corpo da requisição está vazio"));
            return;
        }

        List<CadastroR29VO> listStatus = this.processJson(ctx, new TypeReference<List<CadastroR29VO>>() {
        });
        if (listStatus != null) {
            listStatus.forEach(dados -> {
                try {
                    if (R29_DAO.delete(conn, schema, dados)) {
                        listRetorno.add(new RetornoVO(DELETE, true, "Dados Id [" + dados.getId() + "] deletado com sucesso", null));
                    } else {
                        listRetorno.add(new RetornoVO(DELETE, false, "Dados Id [" + dados.getId() + "] sem correspondente para remoção", null));
                    }
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR29.class.getName()).log(Level.SEVERE, "Erro ao remover Dados", ex);
                    listRetorno.add(new RetornoVO(DELETE, false, null, DAOErrorConverter.convertSQLException(ex)));
                }
            });
        }
        ctx.json(listRetorno);

    }

public void creatingFile(Context ctx) throws UnsupportedEncodingException, IOException {
        listRetorno.clear();
        if (ctx.body().isEmpty()) {
            ctx.json(new RetornoVO(INSERT, false, null, "Corpo da requisição está vazio"));
        }

        if (ctx.body().contains("relatorios")) {
            String fileName = "relatorio.txt";
            FileWriter fileWriter = new FileWriter(fileName);
            InputStream inputStream = new FileInputStream(fileName);
            OutputStream outputStream = ctx.outputStream();

            byte[] request = ctx.bodyAsBytes();
            String jsonString = new String(request, "UTF-8");
            JSONObject json = new JSONObject(jsonString);
            JSONArray arrayDosIds = json.getJSONArray("relatorios");
            
            for (int i = 0; i < arrayDosIds.toList().size(); i++) {
                try {                    
                    JSONObject object = arrayDosIds.getJSONObject(i);                    
                    int id = object.getInt("id");

                    FilterVO filter = new FilterVO("id", Types.BIGINT, String.valueOf(id));
                    List<CadastroR29VO> responseRelatorio = R29_DAO.get(conn, schema, Arrays.asList(filter));

                    CadastroR29VO relatorio = responseRelatorio.get(0);
                    fileWriter.write(relatorio.getR29()== null ? "R29" : relatorio.getR29());                    
                    fileWriter.write(relatorio.getCnpjDeclarante()== null ? "00000000000000" : relatorio.getCnpjDeclarante());
                    fileWriter.write(relatorio.getCnpjSucedida()== null ? "              " : relatorio.getCnpjSucedida());
                    fileWriter.write(relatorio.getDataInicial()== null ? "00000000000000" : relatorio.getDataInicial());
                    fileWriter.write(relatorio.getDataFinal()== null ? "00000000000000" : relatorio.getDataFinal());
                    fileWriter.write(relatorio.getCnpjFontePagadora()== null ? "0000" : relatorio.getCnpjFontePagadora());
                    fileWriter.write(relatorio.getCodigoReceita()== null ? "00" : relatorio.getCodigoReceita());
                    fileWriter.write(relatorio.getVariacao()== null ? "1" : relatorio.getVariacao());
                    fileWriter.write(relatorio.getRetencaoEfetuada()== null ? "00000000000000" : relatorio.getRetencaoEfetuada());
                    fileWriter.write(relatorio.getValor()== null ? "0000000000000" : relatorio.getValor());
                    
                   
                    
                    
                    
                    
                    fileWriter.write("\n");

                } catch (IOException ex) {
                    Logger.getLogger(CadastroR11.class.getName()).log(Level.SEVERE, null, ex);
                } catch (SQLException ex) {
                    Logger.getLogger(CadastroR11.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            fileWriter.close();

            ctx.contentType("text/plain");
            ctx.header("Content-Disposition", "attachment; filename=" + fileName);

            byte[] buffer = new byte[4096];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }

            inputStream.close();
            outputStream.close();

        }
    }

}



