/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR13;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR13Route extends Route {

    public CadastroR13Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR13Route();
    }

    private void createCadastroR13Route() {

        final CadastroR13 cadastroR13 = new CadastroR13(connectionFactory, DEFAULT_SCHEMA);
        final String R13_PATH = defaultPrefixPath("r13");
        final String R13_TXT_PATH = R13_PATH + "/txt";

        //app.get(R13_PATH, ctx -> cadastroR13.getAll(ctx));
        app.get(R13_TXT_PATH, ctx -> cadastroR13.creatingFile(ctx));
        app.get(R13_PATH, ctx -> cadastroR13.get(ctx));
        app.post(R13_PATH, ctx -> cadastroR13.insert(ctx));
        app.patch(R13_PATH, ctx -> cadastroR13.update(ctx));
        app.delete(R13_PATH, ctx -> cadastroR13.delete(ctx));
    }

}
