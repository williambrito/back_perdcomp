/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR12;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR12Route extends Route {

    public CadastroR12Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR12Route();
    }

    private void createCadastroR12Route() {

        final CadastroR12 cadastroR12 = new CadastroR12(connectionFactory, DEFAULT_SCHEMA);
        final String R12_PATH = defaultPrefixPath("r12");
        final String R12_TXT_PATH = R12_PATH + "/txt";
        

        //app.get(R12_PATH, ctx -> cadastroR12.getAll(ctx));
        app.get(R12_TXT_PATH, ctx -> cadastroR12.creatingFile(ctx));
        app.get(R12_PATH, ctx -> cadastroR12.get(ctx));
        app.post(R12_PATH, ctx -> cadastroR12.insert(ctx));
        app.patch(R12_PATH, ctx -> cadastroR12.update(ctx));
        app.delete(R12_PATH, ctx -> cadastroR12.delete(ctx));
    }

}
