/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.server.route;

import br.com.taxationmind.database.dao.ConnectionFactoryPool;
import br.com.taxationmind.server.controller.cadastro.CadastroR11;
import io.javalin.Javalin;

/**
 *
 * @author William Brito
 */
public class CadastroR11Route extends Route {

    public CadastroR11Route(Javalin app, ConnectionFactoryPool connectionFactory, String userSchema) {
        super(app, connectionFactory, userSchema);
        createCadastroR11Route();
    }

    private void createCadastroR11Route() {

        final CadastroR11 cadastroR11 = new CadastroR11(connectionFactory, DEFAULT_SCHEMA);
        final String R11_PATH = defaultPrefixPath("r11");
        final String R11_TXT_PATH = R11_PATH + "/txt";

        //app.get(R11_PATH, ctx -> cadastroR11.getAll(ctx));
        app.get(R11_TXT_PATH, ctx -> cadastroR11.creatingFile(ctx));
        app.get(R11_PATH, ctx -> cadastroR11.get(ctx));
        app.post(R11_PATH, ctx -> cadastroR11.insert(ctx));
        app.patch(R11_PATH, ctx -> cadastroR11.update(ctx));
        app.delete(R11_PATH, ctx -> cadastroR11.delete(ctx));
    }

}
