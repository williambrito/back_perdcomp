/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package br.com.taxationmind.database.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sql.DataSource;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.ConnectionFactory;
import org.apache.commons.dbcp2.DriverManagerConnectionFactory;
import org.apache.commons.dbcp2.PoolableConnection;
import org.apache.commons.dbcp2.PoolableConnectionFactory;
import org.apache.commons.dbcp2.PoolingDriver;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPool;

/**
 *
 * @author William Brito
 */
public class ConnectionFactoryPool {

    private static ConnectionFactoryPool instance = null;
    private DataSource ds;
    private static final String BASE_URL_PG = "jdbc:postgresql:";
    private static final String APPLICATION_NAME_PARAMTER = "?ApplicationName=perdcomp_homolog";

    private ConnectionFactoryPool() {
    }

    public static ConnectionFactoryPool getInstance(String url, String userName, String password) {
        if (instance == null) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ConnectionFactoryPool.class.getName()).log(Level.SEVERE, "Erro ao carregar Driver do PostgreSQL", ex);
            }

            instance = new ConnectionFactoryPool();
            try {
                setupDriver(BASE_URL_PG + url + APPLICATION_NAME_PARAMTER, userName, password);
            } catch (Exception ex) {
                Logger.getLogger(ConnectionFactoryPool.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        return instance;
    }

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection("jdbc:apache:commons:dbcp:perdcomp_homolog");
    }

    private static DataSource setupDataSource(String connectURI, String userName, String password) {
        BasicDataSource ds = new BasicDataSource();
        ds.setUrl(connectURI);
        ds.setUsername(userName);
        ds.setPassword(password);
        ds.setMinIdle(40);
        ds.setMaxIdle(80);
        ds.setMaxTotal(160);
        ds.setPoolPreparedStatements(true);
        ds.setMaxOpenPreparedStatements(300);

        return ds;
    }

    private static void setupDriver(String connectURI, String userName, String password) throws Exception {
        ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(connectURI, userName, password);

        PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory, null);

        ObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);

        poolableConnectionFactory.setPool(connectionPool);

        //
        // Finally, we create the PoolingDriver itself...
        //
        Class.forName("org.apache.commons.dbcp2.PoolingDriver");
        PoolingDriver driver = (PoolingDriver) DriverManager.getDriver("jdbc:apache:commons:dbcp:");

        //
        // ...and register our pool with it.
        //
        driver.registerPool("perdcomp_homolog", connectionPool);

        //
        // Now we can just use the connect string "jdbc:apache:commons:dbcp:example"
        // to access our pool of Connections.
        //
    }
}
