/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR21VO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nathalia Cruz
 */
public class R21DAO extends ModelDAO<CadastroR21VO> {

    @Override
    public boolean insert(ConnectionFactoryPool connF, String schema,CadastroR21VO element) throws SQLException, NullPointerException {
        boolean insert = false;
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("INSERT INTO %s.r21 (r21, cnpj_declarante, cnpj_sucedida, cnpj_detentor, ano_apuracao, mes_apuracao, decendio, movimento, credito_nacional, credito_externo, credito_estorno, credito_presumido, credito_extemporaneos, credito_demais, debito_nacional, debito_estorno, demonstrativo_ressarcimento, demonstrativo_debitos) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? , ?, ? , ? , ?) RETURNING id", schema))) {
            ps.setObject(1, element.getR21(), Types.VARCHAR);
            ps.setObject(2, element.getCnpjDeclarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpjSucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpjDetentor(), Types.VARCHAR);
            ps.setObject(5, element.getAnoApuracao(), Types.VARCHAR);
            ps.setObject(6, element.getMesApuracao(), Types.VARCHAR);
            ps.setObject(7, element.getDecendio(), Types.VARCHAR);
            ps.setObject(8, element.getMovimento(), Types.VARCHAR);
            ps.setObject(9, element.getCreditoNacional(), Types.VARCHAR);
            ps.setObject(10, element.getCreditoExterno(), Types.VARCHAR);
            ps.setObject(11, element.getCreditoEstorno(), Types.VARCHAR);
            ps.setObject(12, element.getCreditoPresumido(), Types.VARCHAR);
            ps.setObject(13, element.getCreditoExtemporaneos(), Types.VARCHAR);
            ps.setObject(14, element.getCreditoDemais(), Types.VARCHAR);
            ps.setObject(15, element.getDebitoNacional(), Types.VARCHAR);
            ps.setObject(16, element.getDebitoEstorno(), Types.VARCHAR);
            ps.setObject(17, element.getDemonstrativoRessarcimento(), Types.VARCHAR);
            ps.setObject(18, element.getDemonstrativoDebitos(), Types.VARCHAR);


            if (ps.execute()) {
                try ( ResultSet rs = ps.getResultSet()) {
                    if (rs.next()) {
                        insert = true;
                    }
                }
            }
        }
        return insert;
    }

    @Override
    public boolean update(ConnectionFactoryPool connF, String schema,CadastroR21VO element) throws SQLException, NullPointerException {
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("UPDATE %s.r21 set r21=?, cnpj_declarante=?, cnpj_sucedida=?, cnpj_detentor=?, ano_apuracao=?, mes_apuracao=?, decendio=?, credito_nacional=?, credito_externo=?, credito_estorno=?, credito_presumido=?, credito_extemporaneos=?, credito_demais=?,debito_nacional=?,,debito_estorno=?,demonstrativo_ressarcimento=? ,demonstrativo_debitos=?  where id=?", schema))) {
            ps.setObject(1, element.getR21(), Types.VARCHAR);
            ps.setObject(2, element.getCnpjDeclarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpjSucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpjDetentor(), Types.VARCHAR);
            ps.setObject(5, element.getAnoApuracao(), Types.VARCHAR);
            ps.setObject(6, element.getMesApuracao(), Types.VARCHAR);
            ps.setObject(7, element.getDecendio(), Types.VARCHAR);
            ps.setObject(8, element.getMovimento(), Types.VARCHAR);
            ps.setObject(9, element.getCreditoNacional(), Types.VARCHAR);
            ps.setObject(10, element.getCreditoExterno(), Types.VARCHAR);
            ps.setObject(11, element.getCreditoEstorno(), Types.VARCHAR);
            ps.setObject(12, element.getCreditoPresumido(), Types.VARCHAR);
            ps.setObject(13, element.getCreditoExtemporaneos(), Types.VARCHAR);
            ps.setObject(14, element.getCreditoDemais(), Types.VARCHAR);
            ps.setObject(15, element.getDebitoNacional(), Types.VARCHAR);
            ps.setObject(16, element.getDebitoEstorno(), Types.VARCHAR);
            ps.setObject(17, element.getDemonstrativoRessarcimento(), Types.VARCHAR);
            ps.setObject(18, element.getDemonstrativoDebitos(), Types.VARCHAR);
            ps.setObject(19, element.getId(), Types.BIGINT);

            return ps.executeUpdate() > 0;
        }
    }

   
        
    @Override
    public boolean delete(ConnectionFactoryPool connF, String schema,CadastroR21VO element) throws SQLException, NullPointerException {
        boolean deleted = false;

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("DELETE FROM %s.r21 where id=?", schema))) {
            ps.setLong(1, element.getId());

            deleted = ps.executeUpdate() > 0;
        }

        return deleted;
    }

    @Override
    public boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CadastroR21VO> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException {
        return get(connF, schema, new ArrayList<>());
    }

    @Override
    public List<CadastroR21VO> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException {
        List<CadastroR21VO> listComp = new ArrayList<>();
        QueryFilterVO q = transformFilter(filterObject);
        String query = String.format("select * from %s.r21 %s", schema, q.getParams());

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(query)) {
            for (Map.Entry<Integer, Object> entry : q.getValues().entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CadastroR21VO comp = new CadastroR21VO();
                    comp.setId(rs.getLong("id"));
                    comp.setR21(rs.getString("r21"));
                    comp.setCnpjDeclarante(rs.getString("cnpj_declarante"));
                    comp.setCnpjSucedida(rs.getString("cnpj_sucedida"));
                    comp.setCnpjDetentor(rs.getString("cnpj_detentor"));
                    comp.setAnoApuracao(rs.getString("ano_apuracao"));
                    comp.setDecendio(rs.getString("decendio"));
                    comp.setMovimento(rs.getString("movimento"));
                    comp.setCreditoNacional(rs.getString("credito_nacional"));
                    comp.setCreditoExterno(rs.getString("credito_externo"));
                    comp.setCreditoEstorno(rs.getString("credito_estorno"));
                    comp.setCreditoPresumido(rs.getString("credito_presumido"));
                    comp.setCreditoExtemporaneos(rs.getString("credito_extemporaneos"));
                    comp.setCreditoDemais(rs.getString("credito_demais"));
                    comp.setDebitoNacional(rs.getString("debito_nacional"));
                    comp.setDebitoEstorno(rs.getString("debito_estorno"));
                    comp.setDemonstrativoRessarcimento(rs.getString("demonstrativo_ressarcimento"));
                    comp.setDemonstrativoDebitos(rs.getString("demonstrativo_debitos"));
                    
                    
                   

                    listComp.add(comp);
                }
            }
        }

        return listComp;
    }

}

