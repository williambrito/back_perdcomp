/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR13VO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;





/**
 *
 * @author Nathalia Cruz
 */
public class R13DAO extends ModelDAO<CadastroR13VO> {

    @Override
    public boolean insert(ConnectionFactoryPool connF, String schema,CadastroR13VO element) throws SQLException, NullPointerException {
        boolean insert = false;
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("INSERT INTO %s.r13 (r13, cnpj_declarante, cnpj_sucedida, cnpj_detentor, cnpj_emitente, num_nota, serie, data_emissao, data_entrada, cfop, valor_total, valor_ipi_destacado, valor_ipi_credito) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id", schema))) {
            ps.setObject(1, element.getR13(), Types.VARCHAR);
            ps.setObject(2, element.getCnpjDeclarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpjSucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpjDetentor(), Types.VARCHAR);
            ps.setObject(5, element.getCnpjEmitente(), Types.VARCHAR);
            ps.setObject(6, element.getNumNota(), Types.VARCHAR);
            ps.setObject(7, element.getSerie(), Types.VARCHAR);
            ps.setObject(8, element.getDataEmissao(), Types.VARCHAR);
            ps.setObject(9, element.getDataEntrada(), Types.VARCHAR);
            ps.setObject(10, element.getCfop(), Types.VARCHAR);
            ps.setObject(11, element.getValorTotal(), Types.VARCHAR);
            ps.setObject(12, element.getValorIpiDestacado(), Types.VARCHAR);
            ps.setObject(13, element.getValorIpiCreditado(), Types.VARCHAR);
           


            if (ps.execute()) {
                try ( ResultSet rs = ps.getResultSet()) {
                    if (rs.next()) {
                        insert = true;
                    }
                }
            }
        }
        return insert;
    }

    @Override
    public boolean update(ConnectionFactoryPool connF, String schema,CadastroR13VO element) throws SQLException, NullPointerException {
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("UPDATE %s.r13 set r13=?, cnpj_declarante=?, cnpj_sucedida=?, cnpj_detentor=?, cnpj_emitente=?, num_nota=?, serie=?, data_emissao=?, data_entrada=?, cfop=?, valor_total=?, valor_ipi_destacado=?, valor_ipi_credito=? id=?", schema))) {
            ps.setObject(1, element.getR13(), Types.VARCHAR);
            ps.setObject(2, element.getCnpjDeclarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpjSucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpjDetentor(), Types.VARCHAR);
            ps.setObject(5, element.getCnpjEmitente(), Types.VARCHAR);
            ps.setObject(6, element.getNumNota(), Types.VARCHAR);
            ps.setObject(7, element.getSerie(), Types.VARCHAR);
            ps.setObject(8, element.getDataEmissao(), Types.VARCHAR);
            ps.setObject(9, element.getDataEntrada(), Types.VARCHAR);
            ps.setObject(10, element.getCfop(), Types.VARCHAR);
            ps.setObject(11, element.getValorTotal(), Types.VARCHAR);
            ps.setObject(12, element.getValorIpiDestacado(), Types.VARCHAR);
            ps.setObject(12, element.getValorIpiCreditado(), Types.VARCHAR);
            ps.setObject(13, element.getId(), Types.BIGINT);

            return ps.executeUpdate() > 0;
        }
    }

   
        
    @Override
    public boolean delete(ConnectionFactoryPool connF, String schema, CadastroR13VO element) throws SQLException, NullPointerException {
        boolean deleted = false;

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("DELETE FROM %s.r13 where id=?", schema))) {
            ps.setLong(1, element.getId());

            deleted = ps.executeUpdate() > 0;
        }

        return deleted;
    }

    @Override
    public boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CadastroR13VO> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException {
        return get(connF, schema, new ArrayList<>());
    }

    @Override
    public List<CadastroR13VO> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException {
        List<CadastroR13VO> listComp = new ArrayList<>();
        QueryFilterVO q = transformFilter(filterObject);
        String query = String.format("select * from %s.r13 %s", schema, q.getParams());

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(query)) {
            for (Map.Entry<Integer, Object> entry : q.getValues().entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CadastroR13VO comp = new CadastroR13VO();
                    comp.setId(rs.getLong("id"));
                    comp.setR13(rs.getString("r13"));
                    comp.setCnpjDeclarante(rs.getString("cnpj_declarante"));
                    comp.setCnpjSucedida(rs.getString("cnpj_sucedida"));
                    comp.setCnpjDetentor(rs.getString("cnpj_detentor"));
                    comp.setCnpjEmitente(rs.getString("cnpj_emitente"));                    
                    comp.setNumNota(rs.getString("num_nota"));
                    comp.setDataEmissao(rs.getString("data_emissao"));
                    comp.setDataEntrada(rs.getString("data_entrada"));
                    comp.setCfop(rs.getString("cfop"));
                    comp.setValorTotal(rs.getString("valor_total"));
                    comp.setValorIpiDestacado(rs.getString("valor_ipi_destacado"));
                    comp.setValorIpiCreditado(rs.getString("valor_ipi_credito"));
                   
                    
                    
                   

                    listComp.add(comp);
                }
            }
        }

        return listComp;
    }

}
