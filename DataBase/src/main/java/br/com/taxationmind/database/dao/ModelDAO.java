/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

/**
 *
 * @author Nathalia Cruz
 * @param <T>
 */
public abstract class ModelDAO<T> {

    private Integer limit = null;

    public static final String CRYPT_PASSWORD = "lax0voh5ru6Auvanohyu9oJ7wohquu8F";

    public abstract boolean insert(ConnectionFactoryPool connF, String schema, T element) throws SQLException, NullPointerException;

    public abstract boolean update(ConnectionFactoryPool connF, String schema, T element) throws SQLException, NullPointerException;

    public abstract boolean delete(ConnectionFactoryPool connF, String schema, T element) throws SQLException, NullPointerException;

    public abstract boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException;

    public abstract List<T> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException;

    public abstract List<T> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException;

    public QueryFilterVO transformFilter(List<FilterVO> filter) {
        int pos = 1;
        String passwordFieldName = "password";
        QueryFilterVO queryFilter = new QueryFilterVO();

        StringBuilder field = new StringBuilder();

        for (FilterVO f : filter) {
            if (f.getField().equals("limit")) {
                limit = Integer.valueOf(f.getValue());
            } else {
                String operator = "=";
                if (f.getClause() != null && f.getClause().equals("IN")) {
                    operator = " = ANY(ARRAY[%s])";
                } else if (f.getTipo() == Types.ARRAY) {
                    operator = " = ANY(?)";
                } else if (f.getTipo() == Types.BOOLEAN) {
                    operator = "=";
                } else if (!f.getField().contains(passwordFieldName) && f.convertValue() instanceof String) {
                    operator = " ilike ";
                } else if (f.convertValue() instanceof java.sql.Date[] || f.convertValue() instanceof Integer[] || f.convertValue() instanceof Long[] || f.convertValue() instanceof Double[]) {
                    operator = " between ";
                } else if (f.convertValue() == null) {
                    operator = " is null ";
                }

                if (field.length() == 0) {
                    field.append(" where ");
                    if (f.getField().contains(passwordFieldName)) {
                        field.append(f.getField()).append(operator).append("crypt(").append("?, ").append(f.getField()).append(")");
                    } else if (operator.contains("ANY")) {
                        if (f.getClause() != null && f.getClause().equals("IN")) {
                            field.append("?").append(String.format(operator, f.getField()));
                        } else {
                            field.append(f.getField()).append(operator);
                        }

                    } else if (operator.equals(" between ")) {
                        field.append(f.getField()).append(operator).append(f.convertValue() == null ? "" : "? and ?");
                    } else {
                        field.append(f.getField()).append(operator).append(f.convertValue() == null ? "" : "?");
                    }
                } else {
                    if (f.getField().contains(passwordFieldName)) {
                        field.append(" and ").append(f.getField()).append(operator).append("crypt(").append("?, ").append(f.getField()).append(")");
                    } else if (operator.contains("ANY")) {
                        if (f.getClause() != null && f.getClause().equals("IN")) {
                            field.append(" and ").append("?").append(String.format(operator, f.getField()));
                        } else {
                            field.append(" and ").append(f.getField()).append(operator);
                        }
                    } else if (operator.equals(" between ")) {
                        field.append(" and ").append(f.getField()).append(operator).append(f.convertValue() == null ? "" : "? and ?");
                    } else {
                        field.append(" and ").append(f.getField()).append(operator).append(f.convertValue() == null ? "" : "?");
                    }
                }

                if (f.convertValue() != null) {
                    if (f.convertValue() instanceof java.sql.Date[]) {
                        for (java.sql.Date d : (java.sql.Date[]) f.convertValue()) {
                            queryFilter.getValues().put(pos++, d);
                        }
                    } else if (f.convertValue() instanceof Integer[]) {
                        for (Integer d : (Integer[]) f.convertValue()) {
                            queryFilter.getValues().put(pos++, d);
                        }
                    } else if (f.convertValue() instanceof Long[]) {
                        for (Long d : (Long[]) f.convertValue()) {
                            queryFilter.getValues().put(pos++, d);
                        }
                    } else if (f.convertValue() instanceof Double[]) {
                        for (Double d : (Double[]) f.convertValue()) {
                            queryFilter.getValues().put(pos++, d);
                        }
                    } else {
                        queryFilter.getValues().put(pos++, f.convertValue());
                    }
                }
            }
        }

        queryFilter.setParams(field.toString());

        return queryFilter;

    }

    public Integer getLimit() {
        return limit;
    }

}