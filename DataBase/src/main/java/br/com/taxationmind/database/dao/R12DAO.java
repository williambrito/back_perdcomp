/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR12VO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;



/**
 *
 * @author Nathalia Cruz
 */
public class R12DAO extends ModelDAO<CadastroR12VO> {

    @Override
    public boolean insert(ConnectionFactoryPool connF, String schema,CadastroR12VO element) throws SQLException, NullPointerException {
        boolean insert = false;
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("INSERT INTO %s.r12 (r12, cnpj_declarante, cnpj_sucedida, cnpj_detentor, ano_apuracao, mes_apuracao, decendio, cfop, base_calculo, ipi_debitado, isentas_nao_tributada, outras) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id", schema))) {
            ps.setObject(1, element.getR12(), Types.VARCHAR);
            ps.setObject(2, element.getCnpjDeclarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpjSucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpjDetentor(), Types.VARCHAR);
            ps.setObject(5, element.getAnoApuracao(), Types.VARCHAR);
            ps.setObject(6, element.getMesApuracao(), Types.VARCHAR);
            ps.setObject(7, element.getDecendio(), Types.VARCHAR);
            ps.setObject(8, element.getCfop(), Types.VARCHAR);
            ps.setObject(9, element.getBaseCalculo(), Types.VARCHAR);
            ps.setObject(10, element.getIpiDebitado(), Types.VARCHAR);
            ps.setObject(11, element.getIsentasNaotributada(), Types.VARCHAR);
            ps.setObject(12, element.getOutras(), Types.VARCHAR);
           


            if (ps.execute()) {
                try ( ResultSet rs = ps.getResultSet()) {
                    if (rs.next()) {
                        insert = true;
                    }
                }
            }
        }
        return insert;
    }

    @Override
    public boolean update(ConnectionFactoryPool connF, String schema, CadastroR12VO element) throws SQLException, NullPointerException {
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("UPDATE %s.r12 set r12=?, cnpj_declarante=?, cnpj_sucedida=?, cnpj_detentor=?, ano_apuracao=?, mes_apuracao=?, decendio=?, cfop=?, base_calculo=?, ipi_debitado=?, isentas_nao_tributadas=?, outras=? id=?", schema))) {
            ps.setObject(1, element.getR12(), Types.VARCHAR);
            ps.setObject(2, element.getCnpjDeclarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpjSucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpjDetentor(), Types.VARCHAR);
            ps.setObject(5, element.getAnoApuracao(), Types.VARCHAR);
            ps.setObject(6, element.getMesApuracao(), Types.VARCHAR);
            ps.setObject(7, element.getDecendio(), Types.VARCHAR);
            ps.setObject(8, element.getCfop(), Types.VARCHAR);
            ps.setObject(9, element.getBaseCalculo(), Types.VARCHAR);
            ps.setObject(10, element.getIpiDebitado(), Types.VARCHAR);
            ps.setObject(11, element.getIsentasNaotributada(), Types.VARCHAR);
            ps.setObject(12, element.getOutras(), Types.VARCHAR);
            ps.setObject(13, element.getId(), Types.BIGINT);

            return ps.executeUpdate() > 0;
        }
    }

   
        
    @Override
    public boolean delete(ConnectionFactoryPool connF, String schema, CadastroR12VO element) throws SQLException, NullPointerException {
        boolean deleted = false;

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("DELETE FROM %s.r12 where id=?", schema))) {
            ps.setLong(1, element.getId());

            deleted = ps.executeUpdate() > 0;
        }

        return deleted;
    }

    @Override
    public boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CadastroR12VO> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException {
        return get(connF, schema, new ArrayList<>());
    }

    @Override
    public List<CadastroR12VO> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException {
        List<CadastroR12VO> listComp = new ArrayList<>();
        QueryFilterVO q = transformFilter(filterObject);
        String query = String.format("select * from %s.r12 %s", schema, q.getParams());

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(query)) {
            for (Map.Entry<Integer, Object> entry : q.getValues().entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CadastroR12VO comp = new CadastroR12VO();
                    comp.setId(rs.getLong("id"));
                    comp.setR12(rs.getString("r12"));
                    comp.setCnpjDeclarante(rs.getString("cnpj_declarante"));
                    comp.setCnpjSucedida(rs.getString("cnpj_sucedida"));
                    comp.setCnpjDetentor(rs.getString("cnpj_detentor"));
                    comp.setAnoApuracao(rs.getString("ano_apuracao"));
                    comp.setDecendio(rs.getString("decendio"));
                    comp.setCfop(rs.getString("cfop"));
                    comp.setBaseCalculo(rs.getString("base_calculo"));
                    comp.setIpiDebitado(rs.getString("ipi_debitado"));
                    comp.setIsentasNaotributada(rs.getString("isentas_nao_tributada"));
                    comp.setOutras(rs.getString("outras"));
                   
                    
                    
                   

                    listComp.add(comp);
                }
            }
        }

        return listComp;
    }
   
}

   
