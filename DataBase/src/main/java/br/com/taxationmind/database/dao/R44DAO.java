/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR44VO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;












/**
 *
 * @author Nathalia Cruz
 */
public class R44DAO extends ModelDAO<CadastroR44VO> {

    @Override
    public boolean insert(ConnectionFactoryPool connF, String schema,CadastroR44VO element) throws SQLException, NullPointerException {
        boolean insert = false;
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("INSERT INTO %s.r44 ( r44, cnpj_declarante, cnpj_sucedida, trimestre, ano_calendario, cnpj_fonte_pagadora, mes, valor_retido) VALUES (?, ?, ?, ?, ?, ?, ?, ?) RETURNING id", schema))) {
            ps.setObject(1,element.getR44(),Types.VARCHAR );
            ps.setObject(2, element.getCnpj_declarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpj_sucedida(), Types.VARCHAR);
            ps.setObject(4, element.getTrimestre(), Types.VARCHAR);
            ps.setObject(5, element.getAno_calendario(), Types.VARCHAR);
            ps.setObject(6, element.getCnpj_fonte_pagadora(), Types.VARCHAR);
            ps.setObject(7, element.getMes(), Types.VARCHAR);
            ps.setObject(8, element.getValor_retido(), Types.VARCHAR);
            
           
            
           


            if (ps.execute()) {
                try ( ResultSet rs = ps.getResultSet()) {
                    if (rs.next()) {
                        insert = true;
                    }
                }
            }
        }
        return insert;
    }

    @Override
    public boolean update(ConnectionFactoryPool connF, String schema,CadastroR44VO element) throws SQLException, NullPointerException {
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("UPDATE %s.r44 set r44=?, r44=?, cnpj_declarante=?, cnpj_sucedida=?, trimestre=?, ano_calendario=?, cnpj_fonte_pagadora=?, mes=?, valor_retido=? where id=?", schema))) {
            ps.setObject(1,element.getR44(),Types.VARCHAR);
            ps.setObject(2, element.getCnpj_declarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpj_sucedida(), Types.VARCHAR);
            ps.setObject(4, element.getTrimestre(), Types.VARCHAR);
            ps.setObject(5, element.getAno_calendario(), Types.VARCHAR);
            ps.setObject(6, element.getCnpj_fonte_pagadora(), Types.VARCHAR);
            ps.setObject(7, element.getMes(), Types.VARCHAR);
            ps.setObject(8, element.getValor_retido(), Types.VARCHAR);            
            ps.setObject(9, element.getId(), Types.BIGINT);

            return ps.executeUpdate() > 0;
        }
    }

   
        
    @Override
    public boolean delete(ConnectionFactoryPool connF, String schema,CadastroR44VO element) throws SQLException, NullPointerException {
        boolean deleted = false;

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("DELETE FROM %s.r44 where id=?", schema))) {
            ps.setLong(1, element.getId());

            deleted = ps.executeUpdate() > 0;
        }

        return deleted;
    }

    @Override
    public boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CadastroR44VO> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException {
        return get(connF, schema, new ArrayList<>());
    }

    @Override
    public List<CadastroR44VO> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException {
        List<CadastroR44VO> listComp = new ArrayList<>();
        QueryFilterVO q = transformFilter(filterObject);
        String query = String.format("select * from %s.r44 %s", schema, q.getParams());

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(query)) {
            for (Map.Entry<Integer, Object> entry : q.getValues().entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CadastroR44VO comp = new CadastroR44VO();
                    comp.setId(rs.getLong("id"));
                    comp.setR44(rs.getString("r44"));
                    comp.setCnpj_declarante(rs.getString("cnpj_declarante"));
                    comp.setCnpj_sucedida(rs.getString("cnpj_sucedida"));
                    comp.setTrimestre(rs.getString("trimestre"));
                    comp.setAno_calendario(rs.getString("ano_calendario"));
                    comp.setCnpj_fonte_pagadora(rs.getString("cnpj_fonte_pagadora"));
                    comp.setMes(rs.getString("mes"));
                    comp.setValor_retido(rs.getString("valor_retido"));
                    
                   
                   
                    
                    
                   

                    listComp.add(comp);
                }
            }
        }

        return listComp;
    }

}


