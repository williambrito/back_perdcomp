/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import br.com.taxationmind.livros.vo.cadastro.BookDataVO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author William Brito
 */
public class BookDataDAO extends ModelDAO<BookDataVO>{
    @Override
    public boolean insert(ConnectionFactoryPool connF, String schema, BookDataVO element) throws SQLException, NullPointerException {
        boolean insert = false;
        try (Connection conn = connF.getConnection(); PreparedStatement ps = conn.prepareStatement(String.format("INSERT INTO %s.book_data ( book_id ,data, status) values( ?, ? , ?) RETURNING id", schema))) {
            ps.setObject(1, element.getBook_id(), Types.INTEGER);
            ps.setObject(2, element.getData(), Types.VARCHAR);
            ps.setObject(3, element.getStatus(), Types.BOOLEAN);
                                    
            if (ps.execute()) {
                try (ResultSet rs = ps.getResultSet()) {
                    if (rs.next()) {
                        insert = true;
                    }
                }
            }
        }
        return insert;
    }

    @Override
    public boolean update(ConnectionFactoryPool connF, String schema, BookDataVO element) throws SQLException, NullPointerException {
        try (Connection conn = connF.getConnection(); PreparedStatement ps = conn.prepareStatement(String.format("UPDATE %s.book_data set book_id=?, data=? , status=?  where id=?", schema))) {
            ps.setObject(1, element.getId(), Types.VARCHAR);
            ps.setObject(2, element.getBook_id(), Types.INTEGER);
            ps.setObject(3, element.getData(), Types.VARCHAR);
            ps.setObject(4, element.getStatus(), Types.BOOLEAN);
            
            

            return ps.executeUpdate() > 0;
        }
    }

    @Override
    public boolean delete(ConnectionFactoryPool connF, String schema, BookDataVO element) throws SQLException, NullPointerException {
        boolean deleted = false;

        try (Connection conn = connF.getConnection(); PreparedStatement ps = conn.prepareStatement(String.format("DELETE FROM %s.book_data where id=?", schema))) {
            ps.setLong(1, element.getId());

            deleted = ps.executeUpdate() > 0;
        }

        return deleted;
    }

    @Override
    public boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<BookDataVO> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException {
        return get(connF, schema, new ArrayList<>());
    }

    @Override
    public List<BookDataVO> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException {
        List<BookDataVO> listComp = new ArrayList<>();
        QueryFilterVO q = transformFilter(filterObject);
        String query = String.format("select * from %s.book_data %s", schema, q.getParams());

        try (Connection conn = connF.getConnection(); PreparedStatement ps = conn.prepareStatement(query)) {
            for (Map.Entry<Integer, Object> entry : q.getValues().entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            try (ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    BookDataVO comp = new BookDataVO();
                    comp.setId(rs.getLong("id"));
                    comp.setBook_id(rs.getLong("book_id"));
                    comp.setData(rs.getString("data"));
                    comp.setStatus(rs.getBoolean("status"));
                    

                    listComp.add(comp);
                }
            }
        }

        return listComp;
    }

}
    
    
