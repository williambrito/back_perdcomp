/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao.utils;

/**
 *
 * @author William Brito
 */
public enum PostgresErrorCode {
    
    C00_SUCCESSFUL_COMPLETION("00000","successful_completion",null, null, null, null),
    C01_WARNING("01000","warning",null, null, null, null),
    C01_DYNAMIC_RESULT_SETS_RETURNED("0100C","dynamic_result_sets_returned",null, null, null, null),
    C01_IMPLICIT_ZERO_BIT_PADDING("01008","implicit_zero_bit_padding",null, null, null, null),
    C01_NULL_VALUE_ELIMINATED_IN_SET_FUNCTION("01003","null_value_eliminated_in_set_function",null, null, null, null),
    C01_PRIVILEGE_NOT_GRANTED("01007","privilege_not_granted",null, null, null, null),
    C01_PRIVILEGE_NOT_REVOKED("01006","privilege_not_revoked",null, null, null, null),
    C01_STRING_DATA_RIGHT_TRUNCATION("01004","string_data_right_truncation",null, null, null, null),
    C01_DEPRECATED_FEATURE("01P01","deprecated_feature",null, null, null, null),
    C02_NO_DATA("02000","no_data",null, null, null, null),
    C02_NO_ADDITIONAL_DYNAMIC_RESULT_SETS_RETURNED("02001","no_additional_dynamic_result_sets_returned",null, null, null, null),
    C03_SQL_STATEMENT_NOT_YET_COMPLETE("03000","sql_statement_not_yet_complete",null, null, null, null),
    C08_CONNECTION_EXCEPTION("08000","connection_exception",null, null, null, null),
    C08_CONNECTION_DOES_NOT_EXIST("08003","connection_does_not_exist",null, null, null, null),
    C08_CONNECTION_FAILURE("08006","connection_failure",null, null, null, null),
    C08_SQLCLIENT_UNABLE_TO_ESTABLISH_SQLCONNECTION("08001","sqlclient_unable_to_establish_sqlconnection",null, null, null, null),
    C08_SQLSERVER_REJECTED_ESTABLISHMENT_OF_SQLCONNECTION("08004","sqlserver_rejected_establishment_of_sqlconnection",null, null, null, null),
    C08_TRANSACTION_RESOLUTION_UNKNOWN("08007","transaction_resolution_unknown",null, null, null, null),
    C08_PROTOCOL_VIOLATION("08P01","protocol_violation",null, null, null, null),
    C09_TRIGGERED_ACTION_EXCEPTION("09000","triggered_action_exception",null, null, null, null),
    C0A_FEATURE_NOT_SUPPORTED("0A000","feature_not_supported",null, null, null, null),
    C0B_INVALID_TRANSACTION_INITIATION("0B000","invalid_transaction_initiation",null, null, null, null),
    C0F_LOCATOR_EXCEPTION("0F000","locator_exception",null, null, null, null),
    C0F_INVALID_LOCATOR_SPECIFICATION("0F001","invalid_locator_specification",null, null, null, null),
    C0L_INVALID_GRANTOR("0L000","invalid_grantor",null, null, null, null),
    C0L_INVALID_GRANT_OPERATION("0LP01","invalid_grant_operation",null, null, null, null),
    C0P_INVALID_ROLE_SPECIFICATION("0P000","invalid_role_specification",null, null, null, null),
    C0Z_DIAGNOSTICS_EXCEPTION("0Z000","diagnostics_exception",null, null, null, null),
    C0Z_STACKED_DIAGNOSTICS_ACCESSED_WITHOUT_ACTIVE_HANDLER("0Z002","stacked_diagnostics_accessed_without_active_handler",null, null, null, null),
    C20_CASE_NOT_FOUND("20000","case_not_found",null, null, null, null),
    C21_CARDINALITY_VIOLATION("21000","cardinality_violation",null, null, null, null),
    C22_DATA_EXCEPTION("22000","data_exception",null, null, null, null),
    C22_ARRAY_SUBSCRIPT_ERROR("2202E","array_subscript_error",null, null, null, null),
    C22_CHARACTER_NOT_IN_REPERTOIRE("22021","character_not_in_repertoire",null, null, null, null),
    C22_DATETIME_FIELD_OVERFLOW("22008","datetime_field_overflow",null, null, null, null),
    C22_DIVISION_BY_ZERO("22012","division_by_zero",null, null, null, null),
    C22_ERROR_IN_ASSIGNMENT("22005","error_in_assignment",null, null, null, null),
    C22_ESCAPE_CHARACTER_CONFLICT("2200B","escape_character_conflict",null, null, null, null),
    C22_INDICATOR_OVERFLOW("22022","indicator_overflow",null, null, null, null),
    C22_INTERVAL_FIELD_OVERFLOW("22015","interval_field_overflow",null, null, null, null),
    C22_INVALID_ARGUMENT_FOR_LOGARITHM("2201E","invalid_argument_for_logarithm",null, null, null, null),
    C22_INVALID_ARGUMENT_FOR_NTILE_FUNCTION("22014","invalid_argument_for_ntile_function",null, null, null, null),
    C22_INVALID_ARGUMENT_FOR_NTH_VALUE_FUNCTION("22016","invalid_argument_for_nth_value_function",null, null, null, null),
    C22_INVALID_ARGUMENT_FOR_POWER_FUNCTION("2201F","invalid_argument_for_power_function",null, null, null, null),
    C22_INVALID_ARGUMENT_FOR_WIDTH_BUCKET_FUNCTION("2201G","invalid_argument_for_width_bucket_function",null, null, null, null),
    C22_INVALID_CHARACTER_VALUE_FOR_CAST("22018","invalid_character_value_for_cast",null, null, null, null),
    C22_INVALID_DATETIME_FORMAT("22007","invalid_datetime_format",null, null, null, null),
    C22_INVALID_ESCAPE_CHARACTER("22019","invalid_escape_character",null, null, null, null),
    C22_INVALID_ESCAPE_OCTET("2200D","invalid_escape_octet",null, null, null, null),
    C22_INVALID_ESCAPE_SEQUENCE("22025","invalid_escape_sequence",null, null, null, null),
    C22_NONSTANDARD_USE_OF_ESCAPE_CHARACTER("22P06","nonstandard_use_of_escape_character",null, null, null, null),
    C22_INVALID_INDICATOR_PARAMETER_VALUE("22010","invalid_indicator_parameter_value",null, null, null, null),
    C22_INVALID_PARAMETER_VALUE("22023","invalid_parameter_value",null, null, null, null),
    C22_INVALID_PRECEDING_OR_FOLLOWING_SIZE("22013","invalid_preceding_or_following_size",null, null, null, null),
    C22_INVALID_REGULAR_EXPRESSION("2201B","invalid_regular_expression",null, null, null, null),
    C22_INVALID_ROW_COUNT_IN_LIMIT_CLAUSE("2201W","invalid_row_count_in_limit_clause",null, null, null, null),
    C22_INVALID_ROW_COUNT_IN_RESULT_OFFSET_CLAUSE("2201X","invalid_row_count_in_result_offset_clause",null, null, null, null),
    C22_INVALID_TABLESAMPLE_ARGUMENT("2202H","invalid_tablesample_argument",null, null, null, null),
    C22_INVALID_TABLESAMPLE_REPEAT("2202G","invalid_tablesample_repeat",null, null, null, null),
    C22_INVALID_TIME_ZONE_DISPLACEMENT_VALUE("22009","invalid_time_zone_displacement_value",null, null, null, null),
    C22_INVALID_USE_OF_ESCAPE_CHARACTER("2200C","invalid_use_of_escape_character",null, null, null, null),
    C22_MOST_SPECIFIC_TYPE_MISMATCH("2200G","most_specific_type_mismatch",null, null, null, null),
    C22_NULL_VALUE_NOT_ALLOWED("22004","null_value_not_allowed",null, null, null, null),
    C22_NULL_VALUE_NO_INDICATOR_PARAMETER("22002","null_value_no_indicator_parameter",null, null, null, null),
    C22_NUMERIC_VALUE_OUT_OF_RANGE("22003","numeric_value_out_of_range",null, null, null, null),
    C22_SEQUENCE_GENERATOR_LIMIT_EXCEEDED("2200H","sequence_generator_limit_exceeded",null, null, null, null),
    C22_STRING_DATA_LENGTH_MISMATCH("22026","string_data_length_mismatch",null, null, null, null),
    C22_STRING_DATA_VALUE_TOO_LONG_FOR_TYPE_VARCHAR("22001","string_data_value_too_long_for_type_varchar","Valor muito longo para o tipo texto", "\\((.*?)\\)", new Integer[]{1}, "[valor com mais de %s caractéres]"),
    C22_SUBSTRING_ERROR("22011","substring_error",null, null, null, null),
    C22_TRIM_ERROR("22027","trim_error",null, null, null, null),
    C22_UNTERMINATED_C_STRING("22024","unterminated_c_string",null, null, null, null),
    C22_ZERO_LENGTH_CHARACTER_STRING("2200F","zero_length_character_string",null, null, null, null),
    C22_FLOATING_POINT_EXCEPTION("22P01","floating_point_exception",null, null, null, null),
    C22_INVALID_TEXT_REPRESENTATION("22P02","invalid_text_representation",null, null, null, null),
    C22_INVALID_BINARY_REPRESENTATION("22P03","invalid_binary_representation",null, null, null, null),
    C22_BAD_COPY_FILE_FORMAT("22P04","bad_copy_file_format",null, null, null, null),
    C22_UNTRANSLATABLE_CHARACTER("22P05","untranslatable_character",null, null, null, null),
    C22_NOT_AN_XML_DOCUMENT("2200L","not_an_xml_document",null, null, null, null),
    C22_INVALID_XML_DOCUMENT("2200M","invalid_xml_document",null, null, null, null),
    C22_INVALID_XML_CONTENT("2200N","invalid_xml_content",null, null, null, null),
    C22_INVALID_XML_COMMENT("2200S","invalid_xml_comment",null, null, null, null),
    C22_INVALID_XML_PROCESSING_INSTRUCTION("2200T","invalid_xml_processing_instruction",null, null, null, null),
    C22_DUPLICATE_JSON_OBJECT_KEY_VALUE("22030","duplicate_json_object_key_value",null, null, null, null),
    C22_INVALID_JSON_TEXT("22032","invalid_json_text",null, null, null, null),
    C22_INVALID_SQL_JSON_SUBSCRIPT("22033","invalid_sql_json_subscript",null, null, null, null),
    C22_MORE_THAN_ONE_SQL_JSON_ITEM("22034","more_than_one_sql_json_item",null, null, null, null),
    C22_NO_SQL_JSON_ITEM("22035","no_sql_json_item",null, null, null, null),
    C22_NON_NUMERIC_SQL_JSON_ITEM("22036","non_numeric_sql_json_item",null, null, null, null),
    C22_NON_UNIQUE_KEYS_IN_A_JSON_OBJECT("22037","non_unique_keys_in_a_json_object",null, null, null, null),
    C22_SINGLETON_SQL_JSON_ITEM_REQUIRED("22038","singleton_sql_json_item_required",null, null, null, null),
    C22_SQL_JSON_ARRAY_NOT_FOUND("22039","sql_json_array_not_found",null, null, null, null),
    C22_SQL_JSON_MEMBER_NOT_FOUND("2203A","sql_json_member_not_found",null, null, null, null),
    C22_SQL_JSON_NUMBER_NOT_FOUND("2203B","sql_json_number_not_found",null, null, null, null),
    C22_SQL_JSON_OBJECT_NOT_FOUND("2203C","sql_json_object_not_found",null, null, null, null),
    C22_TOO_MANY_JSON_ARRAY_ELEMENTS("2203D","too_many_json_array_elements",null, null, null, null),
    C22_TOO_MANY_JSON_OBJECT_MEMBERS("2203E","too_many_json_object_members",null, null, null, null),
    C22_SQL_JSON_SCALAR_REQUIRED("2203F","sql_json_scalar_required",null, null, null, null),
    C23_INTEGRITY_CONSTRAINT_VIOLATION("23000","integrity_constraint_violation",null, null, null, null),
    C23_RESTRICT_VIOLATION("23001","restrict_violation",null, null, null, null),
    C23_NOT_NULL_VIOLATION("23502","not_null_violation","Violação de valor não nulo", "\\\"(.*?)\\\"", new Integer[]{1}, "[Valor nulo encontrado no campo \"%s\"]"),
    C23_FOREIGN_KEY_VIOLATION("23503","foreign_key_violation","Violação de chave estrangeira", "\\((.*?)\\)=\\((.*?)\\).*\\\"(.*?)\\\"", new Integer[]{1,2,3}, "[%s=%s (não) tem referência na tabela %s]"),
    C23_UNIQUE_VIOLATION("23505","unique_violation","Chave Duplicada", "\\((.*?)\\)=\\((.*?)\\)", new Integer[]{1,2}, "[%s=%s]"),
    C23_CHECK_VIOLATION("23514","check_violation",null, null, null, null),
    C23_EXCLUSION_VIOLATION("23P01","exclusion_violation",null, null, null, null),
    C24_INVALID_CURSOR_STATE("24000","invalid_cursor_state",null, null, null, null),
    C25_INVALID_TRANSACTION_STATE("25000","invalid_transaction_state",null, null, null, null),
    C25_ACTIVE_SQL_TRANSACTION("25001","active_sql_transaction",null, null, null, null),
    C25_BRANCH_TRANSACTION_ALREADY_ACTIVE("25002","branch_transaction_already_active",null, null, null, null),
    C25_HELD_CURSOR_REQUIRES_SAME_ISOLATION_LEVEL("25008","held_cursor_requires_same_isolation_level",null, null, null, null),
    C25_INAPPROPRIATE_ACCESS_MODE_FOR_BRANCH_TRANSACTION("25003","inappropriate_access_mode_for_branch_transaction",null, null, null, null),
    C25_INAPPROPRIATE_ISOLATION_LEVEL_FOR_BRANCH_TRANSACTION("25004","inappropriate_isolation_level_for_branch_transaction",null, null, null, null),
    C25_NO_ACTIVE_SQL_TRANSACTION_FOR_BRANCH_TRANSACTION("25005","no_active_sql_transaction_for_branch_transaction",null, null, null, null),
    C25_READ_ONLY_SQL_TRANSACTION("25006","read_only_sql_transaction",null, null, null, null),
    C25_SCHEMA_AND_DATA_STATEMENT_MIXING_NOT_SUPPORTED("25007","schema_and_data_statement_mixing_not_supported",null, null, null, null),
    C25_NO_ACTIVE_SQL_TRANSACTION("25P01","no_active_sql_transaction",null, null, null, null),
    C25_IN_FAILED_SQL_TRANSACTION("25P02","in_failed_sql_transaction",null, null, null, null),
    C25_IDLE_IN_TRANSACTION_SESSION_TIMEOUT("25P03","idle_in_transaction_session_timeout",null, null, null, null),
    C26_INVALID_SQL_STATEMENT_NAME("26000","invalid_sql_statement_name",null, null, null, null),
    C27_TRIGGERED_DATA_CHANGE_VIOLATION("27000","triggered_data_change_violation",null, null, null, null),
    C28_INVALID_AUTHORIZATION_SPECIFICATION("28000","invalid_authorization_specification",null, null, null, null),
    C28_INVALID_PASSWORD("28P01","invalid_password",null, null, null, null),
    C2B_DEPENDENT_PRIVILEGE_DESCRIPTORS_STILL_EXIST("2B000","dependent_privilege_descriptors_still_exist",null, null, null, null),
    C2B_DEPENDENT_OBJECTS_STILL_EXIST("2BP01","dependent_objects_still_exist",null, null, null, null),
    C2D_INVALID_TRANSACTION_TERMINATION("2D000","invalid_transaction_termination",null, null, null, null),
    C2F_SQL_ROUTINE_EXCEPTION("2F000","sql_routine_exception",null, null, null, null),
    C2F_FUNCTION_EXECUTED_NO_RETURN_STATEMENT("2F005","function_executed_no_return_statement",null, null, null, null),
    C2F_MODIFYING_SQL_DATA_NOT_PERMITTED("2F002","modifying_sql_data_not_permitted",null, null, null, null),
    C2F_PROHIBITED_SQL_STATEMENT_ATTEMPTED("2F003","prohibited_sql_statement_attempted",null, null, null, null),
    C2F_READING_SQL_DATA_NOT_PERMITTED("2F004","reading_sql_data_not_permitted",null, null, null, null),
    C34_INVALID_CURSOR_NAME("34000","invalid_cursor_name",null, null, null, null),
    C38_EXTERNAL_ROUTINE_EXCEPTION("38000","external_routine_exception",null, null, null, null),
    C38_CONTAINING_SQL_NOT_PERMITTED("38001","containing_sql_not_permitted",null, null, null, null),
    C38_MODIFYING_SQL_DATA_NOT_PERMITTED("38002","modifying_sql_data_not_permitted",null, null, null, null),
    C38_PROHIBITED_SQL_STATEMENT_ATTEMPTED("38003","prohibited_sql_statement_attempted",null, null, null, null),
    C38_READING_SQL_DATA_NOT_PERMITTED("38004","reading_sql_data_not_permitted",null, null, null, null),
    C39_EXTERNAL_ROUTINE_INVOCATION_EXCEPTION("39000","external_routine_invocation_exception",null, null, null, null),
    C39_INVALID_SQLSTATE_RETURNED("39001","invalid_sqlstate_returned",null, null, null, null),
    C39_NULL_VALUE_NOT_ALLOWED("39004","null_value_not_allowed",null, null, null, null),
    C39_TRIGGER_PROTOCOL_VIOLATED("39P01","trigger_protocol_violated",null, null, null, null),
    C39_SRF_PROTOCOL_VIOLATED("39P02","srf_protocol_violated",null, null, null, null),
    C39_EVENT_TRIGGER_PROTOCOL_VIOLATED("39P03","event_trigger_protocol_violated",null, null, null, null),
    C3B_SAVEPOINT_EXCEPTION("3B000","savepoint_exception",null, null, null, null),
    C3B_INVALID_SAVEPOINT_SPECIFICATION("3B001","invalid_savepoint_specification",null, null, null, null),
    C3D_INVALID_CATALOG_NAME("3D000","invalid_catalog_name",null, null, null, null),
    C3F_INVALID_SCHEMA_NAME("3F000","invalid_schema_name",null, null, null, null),
    C40_TRANSACTION_ROLLBACK("40000","transaction_rollback",null, null, null, null),
    C40_TRANSACTION_INTEGRITY_CONSTRAINT_VIOLATION("40002","transaction_integrity_constraint_violation",null, null, null, null),
    C40_SERIALIZATION_FAILURE("40001","serialization_failure",null, null, null, null),
    C40_STATEMENT_COMPLETION_UNKNOWN("40003","statement_completion_unknown",null, null, null, null),
    C40_DEADLOCK_DETECTED("40P01","deadlock_detected",null, null, null, null),
    C42_SYNTAX_ERROR_OR_ACCESS_RULE_VIOLATION("42000","syntax_error_or_access_rule_violation",null, null, null, null),
    C42_SYNTAX_ERROR("42601","syntax_error",null, null, null, null),
    C42_INSUFFICIENT_PRIVILEGE("42501","insufficient_privilege",null, null, null, null),
    C42_CANNOT_COERCE("42846","cannot_coerce",null, null, null, null),
    C42_GROUPING_ERROR("42803","grouping_error",null, null, null, null),
    C42_WINDOWING_ERROR("42P20","windowing_error",null, null, null, null),
    C42_INVALID_RECURSION("42P19","invalid_recursion",null, null, null, null),
    C42_INVALID_FOREIGN_KEY("42830","invalid_foreign_key",null, null, null, null),
    C42_INVALID_NAME("42602","invalid_name",null, null, null, null),
    C42_NAME_TOO_LONG("42622","name_too_long",null, null, null, null),
    C42_RESERVED_NAME("42939","reserved_name",null, null, null, null),
    C42_DATATYPE_MISMATCH("42804","datatype_mismatch",null, null, null, null),
    C42_INDETERMINATE_DATATYPE("42P18","indeterminate_datatype",null, null, null, null),
    C42_COLLATION_MISMATCH("42P21","collation_mismatch",null, null, null, null),
    C42_INDETERMINATE_COLLATION("42P22","indeterminate_collation",null, null, null, null),
    C42_WRONG_OBJECT_TYPE("42809","wrong_object_type",null, null, null, null),
    C42_GENERATED_ALWAYS("428C9","generated_always",null, null, null, null),
    C42_UNDEFINED_COLUMN("42703","undefined_column",null, null, null, null),
    C42_UNDEFINED_FUNCTION("42883","undefined_function", "Função desconhecida", "function (.*?) does", new Integer[]{1}, "Função %s não existe"),
    C42_UNDEFINED_TABLE("42P01","undefined_table",null, null, null, null),
    C42_UNDEFINED_PARAMETER("42P02","undefined_parameter",null, null, null, null),
    C42_UNDEFINED_OBJECT("42704","undefined_object",null, null, null, null),
    C42_DUPLICATE_COLUMN("42701","duplicate_column",null, null, null, null),
    C42_DUPLICATE_CURSOR("42P03","duplicate_cursor",null, null, null, null),
    C42_DUPLICATE_DATABASE("42P04","duplicate_database",null, null, null, null),
    C42_DUPLICATE_FUNCTION("42723","duplicate_function",null, null, null, null),
    C42_DUPLICATE_PREPARED_STATEMENT("42P05","duplicate_prepared_statement",null, null, null, null),
    C42_DUPLICATE_SCHEMA("42P06","duplicate_schema",null, null, null, null),
    C42_DUPLICATE_TABLE("42P07","duplicate_table",null, null, null, null),
    C42_DUPLICATE_ALIAS("42712","duplicate_alias",null, null, null, null),
    C42_DUPLICATE_OBJECT("42710","duplicate_object",null, null, null, null),
    C42_AMBIGUOUS_COLUMN("42702","ambiguous_column",null, null, null, null),
    C42_AMBIGUOUS_FUNCTION("42725","ambiguous_function",null, null, null, null),
    C42_AMBIGUOUS_PARAMETER("42P08","ambiguous_parameter",null, null, null, null),
    C42_AMBIGUOUS_ALIAS("42P09","ambiguous_alias",null, null, null, null),
    C42_INVALID_COLUMN_REFERENCE("42P10","invalid_column_reference",null, null, null, null),
    C42_INVALID_COLUMN_DEFINITION("42611","invalid_column_definition",null, null, null, null),
    C42_INVALID_CURSOR_DEFINITION("42P11","invalid_cursor_definition",null, null, null, null),
    C42_INVALID_DATABASE_DEFINITION("42P12","invalid_database_definition",null, null, null, null),
    C42_INVALID_FUNCTION_DEFINITION("42P13","invalid_function_definition",null, null, null, null),
    C42_INVALID_PREPARED_STATEMENT_DEFINITION("42P14","invalid_prepared_statement_definition",null, null, null, null),
    C42_INVALID_SCHEMA_DEFINITION("42P15","invalid_schema_definition",null, null, null, null),
    C42_INVALID_TABLE_DEFINITION("42P16","invalid_table_definition",null, null, null, null),
    C42_INVALID_OBJECT_DEFINITION("42P17","invalid_object_definition",null, null, null, null),
    C44_WITH_CHECK_OPTION_VIOLATION("44000","with_check_option_violation",null, null, null, null),
    C53_INSUFFICIENT_RESOURCES("53000","insufficient_resources",null, null, null, null),
    C53_DISK_FULL("53100","disk_full",null, null, null, null),
    C53_OUT_OF_MEMORY("53200","out_of_memory",null, null, null, null),
    C53_TOO_MANY_CONNECTIONS("53300","too_many_connections",null, null, null, null),
    C53_CONFIGURATION_LIMIT_EXCEEDED("53400","configuration_limit_exceeded",null, null, null, null),
    C54_PROGRAM_LIMIT_EXCEEDED("54000","program_limit_exceeded",null, null, null, null),
    C54_STATEMENT_TOO_COMPLEX("54001","statement_too_complex",null, null, null, null),
    C54_TOO_MANY_COLUMNS("54011","too_many_columns",null, null, null, null),
    C54_TOO_MANY_ARGUMENTS("54023","too_many_arguments",null, null, null, null),
    C55_OBJECT_NOT_IN_PREREQUISITE_STATE("55000","object_not_in_prerequisite_state",null, null, null, null),
    C55_OBJECT_IN_USE("55006","object_in_use",null, null, null, null),
    C55_CANT_CHANGE_RUNTIME_PARAM("55P02","cant_change_runtime_param",null, null, null, null),
    C55_LOCK_NOT_AVAILABLE("55P03","lock_not_available",null, null, null, null),
    C55_UNSAFE_NEW_ENUM_VALUE_USAGE("55P04","unsafe_new_enum_value_usage",null, null, null, null),
    C57_OPERATOR_INTERVENTION("57000","operator_intervention",null, null, null, null),
    C57_QUERY_CANCELED("57014","query_canceled",null, null, null, null),
    C57_ADMIN_SHUTDOWN("57P01","admin_shutdown",null, null, null, null),
    C57_CRASH_SHUTDOWN("57P02","crash_shutdown",null, null, null, null),
    C57_CANNOT_CONNECT_NOW("57P03","cannot_connect_now",null, null, null, null),
    C57_DATABASE_DROPPED("57P04","database_dropped",null, null, null, null),
    C58_SYSTEM_ERROR("58000","system_error",null, null, null, null),
    C58_IO_ERROR("58030","io_error",null, null, null, null),
    C58_UNDEFINED_FILE("58P01","undefined_file",null, null, null, null),
    C58_DUPLICATE_FILE("58P02","duplicate_file",null, null, null, null),
    C72_SNAPSHOT_TOO_OLD("72000","snapshot_too_old",null, null, null, null),
    CF0_CONFIG_FILE_ERROR("F0000","config_file_error",null, null, null, null),
    CF0_LOCK_FILE_EXISTS("F0001","lock_file_exists",null, null, null, null),
    CHV_FDW_ERROR("HV000","fdw_error",null, null, null, null),
    CHV_FDW_COLUMN_NAME_NOT_FOUND("HV005","fdw_column_name_not_found",null, null, null, null),
    CHV_FDW_DYNAMIC_PARAMETER_VALUE_NEEDED("HV002","fdw_dynamic_parameter_value_needed",null, null, null, null),
    CHV_FDW_FUNCTION_SEQUENCE_ERROR("HV010","fdw_function_sequence_error",null, null, null, null),
    CHV_FDW_INCONSISTENT_DESCRIPTOR_INFORMATION("HV021","fdw_inconsistent_descriptor_information",null, null, null, null),
    CHV_FDW_INVALID_ATTRIBUTE_VALUE("HV024","fdw_invalid_attribute_value",null, null, null, null),
    CHV_FDW_INVALID_COLUMN_NAME("HV007","fdw_invalid_column_name",null, null, null, null),
    CHV_FDW_INVALID_COLUMN_NUMBER("HV008","fdw_invalid_column_number",null, null, null, null),
    CHV_FDW_INVALID_DATA_TYPE("HV004","fdw_invalid_data_type",null, null, null, null),
    CHV_FDW_INVALID_DATA_TYPE_DESCRIPTORS("HV006","fdw_invalid_data_type_descriptors",null, null, null, null),
    CHV_FDW_INVALID_DESCRIPTOR_FIELD_IDENTIFIER("HV091","fdw_invalid_descriptor_field_identifier",null, null, null, null),
    CHV_FDW_INVALID_HANDLE("HV00B","fdw_invalid_handle",null, null, null, null),
    CHV_FDW_INVALID_OPTION_INDEX("HV00C","fdw_invalid_option_index",null, null, null, null),
    CHV_FDW_INVALID_OPTION_NAME("HV00D","fdw_invalid_option_name",null, null, null, null),
    CHV_FDW_INVALID_STRING_LENGTH_OR_BUFFER_LENGTH("HV090","fdw_invalid_string_length_or_buffer_length",null, null, null, null),
    CHV_FDW_INVALID_STRING_FORMAT("HV00A","fdw_invalid_string_format",null, null, null, null),
    CHV_FDW_INVALID_USE_OF_NULL_POINTER("HV009","fdw_invalid_use_of_null_pointer",null, null, null, null),
    CHV_FDW_TOO_MANY_HANDLES("HV014","fdw_too_many_handles",null, null, null, null),
    CHV_FDW_OUT_OF_MEMORY("HV001","fdw_out_of_memory",null, null, null, null),
    CHV_FDW_NO_SCHEMAS("HV00P","fdw_no_schemas",null, null, null, null),
    CHV_FDW_OPTION_NAME_NOT_FOUND("HV00J","fdw_option_name_not_found",null, null, null, null),
    CHV_FDW_REPLY_HANDLE("HV00K","fdw_reply_handle",null, null, null, null),
    CHV_FDW_SCHEMA_NOT_FOUND("HV00Q","fdw_schema_not_found",null, null, null, null),
    CHV_FDW_TABLE_NOT_FOUND("HV00R","fdw_table_not_found",null, null, null, null),
    CHV_FDW_UNABLE_TO_CREATE_EXECUTION("HV00L","fdw_unable_to_create_execution",null, null, null, null),
    CHV_FDW_UNABLE_TO_CREATE_REPLY("HV00M","fdw_unable_to_create_reply",null, null, null, null),
    CHV_FDW_UNABLE_TO_ESTABLISH_CONNECTION("HV00N","fdw_unable_to_establish_connection",null, null, null, null),
    CP0_PLPGSQL_ERROR("P0000","plpgsql_error",null, null, null, null),
    CP0_RAISE_EXCEPTION("P0001","raise_exception",null, null, null, null),
    CP0_NO_DATA_FOUND("P0002","no_data_found",null, null, null, null),
    CP0_TOO_MANY_ROWS("P0003","too_many_rows",null, null, null, null),
    CP0_ASSERT_FAILURE("P0004","assert_failure",null, null, null, null),
    CXX_INTERNAL_ERROR("XX000","internal_error",null, null, null, null),
    CXX_DATA_CORRUPTED("XX001","data_corrupted",null, null, null, null),
    CXX_INDEX_CORRUPTED("XX002","index_corrupted",null, null, null, null);
    
    private final String code;
    private final String descricaoEn;
    private final String descricaoPtBr;
    private final String regularExpression;
    private final Integer[] position;
    private final String model;
    
    PostgresErrorCode(String code, String descricaoEn, String descricaoPtBr, String regularExpression, Integer[] position, String model) {
        this.code = code;
        this.descricaoEn = descricaoEn;
        this.descricaoPtBr = descricaoPtBr;
        this.regularExpression = regularExpression;
        this.position = position;
        this.model = model;
    }

    public String getCode() {
        return code;
    }

    public String getDescricaoEn() {
        return descricaoEn;
    }
    
    public String getDescricaoPtBr() {
        return descricaoPtBr==null?descricaoEn:descricaoPtBr;
    }
    
    public String getRegularExpression() {
        return regularExpression;
    }
    
    public Integer[] getPosition() {
        return position;
    }
    
    public String getModel() {
        return model;
    }
    
    public static PostgresErrorCode fromCode(String code) {
        for (PostgresErrorCode c : PostgresErrorCode.values()) {
            if (c.getCode().equals(code)) {
                return c;
            }
        }
        return null;
    }
    
}
