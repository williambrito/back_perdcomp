/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR36VO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;








/**
 *
 * @author Nathalia Cruz
 */
public class R36DAO extends ModelDAO<CadastroR36VO> {

    @Override
    public boolean insert(ConnectionFactoryPool connF, String schema,CadastroR36VO element) throws SQLException, NullPointerException {
        boolean insert = false;
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("INSERT INTO %s.r36 ( r36, cnpj_declarante, cnpj_sucedida, data_inicial, data_final, cnpj_fonte_pagadora, codigo_receita, variacao, retencao_efetuada, valor) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) RETURNING id", schema))) {
            ps.setObject(1,element.getR36(),Types.VARCHAR );
            ps.setObject(2, element.getCnpj_declarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpj_sucedida(), Types.VARCHAR);
            ps.setObject(4, element.getData_inicial(), Types.VARCHAR);
            ps.setObject(5, element.getData_final(), Types.VARCHAR);
            ps.setObject(6, element.getCnpj_fonte_pagadora(), Types.VARCHAR);
            ps.setObject(7, element.getCod_receita(), Types.VARCHAR);
            ps.setObject(8, element.getVariacao(), Types.VARCHAR);
            ps.setObject(9, element.getRetencao_efetuada(), Types.VARCHAR);
            ps.setObject(10, element.getValor(), Types.VARCHAR);
            
           


            if (ps.execute()) {
                try ( ResultSet rs = ps.getResultSet()) {
                    if (rs.next()) {
                        insert = true;
                    }
                }
            }
        }
        return insert;
    }

    @Override
    public boolean update(ConnectionFactoryPool connF, String schema,CadastroR36VO element) throws SQLException, NullPointerException {
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("UPDATE %s.r29 set r36=?, cnpj_declarante=?, cnpj_sucedida=?,data_inicial=?, data_final=?, cnpj_fonte_pagadora=?, cod_receita=?, variacao=?, retencao_efetuada=?, valor=?, id=?", schema))) {
            ps.setObject(1,element.getR36(),Types.VARCHAR);
            ps.setObject(2, element.getCnpj_declarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpj_sucedida(), Types.VARCHAR);
            ps.setObject(4, element.getData_inicial(), Types.VARCHAR);
            ps.setObject(5, element.getData_final(), Types.VARCHAR);
            ps.setObject(6, element.getCnpj_fonte_pagadora(), Types.VARCHAR);
            ps.setObject(7, element.getCod_receita(), Types.VARCHAR);
            ps.setObject(8, element.getVariacao(), Types.VARCHAR);
            ps.setObject(9, element.getRetencao_efetuada(), Types.VARCHAR);
            ps.setObject(10, element.getValor(), Types.VARCHAR);
            ps.setObject(11, element.getId(), Types.BIGINT);

            return ps.executeUpdate() > 0;
        }
    }

   
        
    @Override
    public boolean delete(ConnectionFactoryPool connF, String schema,CadastroR36VO element) throws SQLException, NullPointerException {
        boolean deleted = false;

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("DELETE FROM %s.r36 where id=?", schema))) {
            ps.setLong(1, element.getId());

            deleted = ps.executeUpdate() > 0;
        }

        return deleted;
    }

    @Override
    public boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CadastroR36VO> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException {
        return get(connF, schema, new ArrayList<>());
    }

    @Override
    public List<CadastroR36VO> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException {
        List<CadastroR36VO> listComp = new ArrayList<>();
        QueryFilterVO q = transformFilter(filterObject);
        String query = String.format("select * from %s.r36 %s", schema, q.getParams());

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(query)) {
            for (Map.Entry<Integer, Object> entry : q.getValues().entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CadastroR36VO comp = new CadastroR36VO();
                    comp.setId(rs.getLong("id"));
                    comp.setR36(rs.getString("r36"));
                    comp.setCnpj_declarante(rs.getString("cnpj_declarante"));
                    comp.setCnpj_sucedida(rs.getString("cnpj_sucedida"));
                    comp.setData_inicial(rs.getString("data_inicial"));
                    comp.setData_final(rs.getString("data_final"));
                    comp.setCnpj_fonte_pagadora(rs.getString("cnpj_fonte_pagadora"));
                    comp.setCod_receita(rs.getString("codigo_receita"));
                    comp.setVariacao(rs.getString("variacao"));
                    comp.setRetencao_efetuada(rs.getString("retencao_efetuada"));
                    comp.setValor(rs.getString("valor"));
                   
                   
                    
                    
                   

                    listComp.add(comp);
                }
            }
        }

        return listComp;
    }

}

