/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao;

import br.com.taxationmind.livros.vo.FilterVO;
import br.com.taxationmind.livros.vo.QueryFilterVO;
import br.com.taxationmind.livros.vo.cadastro.CadastroR15VO;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;






/**
 *
 * @author Nathalia Cruz
 */
public class R15DAO extends ModelDAO<CadastroR15VO> {

    @Override
    public boolean insert(ConnectionFactoryPool connF, String schema,CadastroR15VO element) throws SQLException, NullPointerException {
        boolean insert = false;
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("INSERT INTO %s.r15 (r15, cnpj_declarante, cnpj_sucedida, cnpj_detentor, cnpj_emitente, num_nota, serie, data_emissao, data_entrada, cfop, valor_total, valor_ipi_destacado, valor_ipi_creditado, especie_cred, decendio, mes_periodo, ano_periodo) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) RETURNING id", schema))) {
            ps.setObject(1, element.getR15(), Types.VARCHAR);
            ps.setObject(2, element.getCnpj_declarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpj_sucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpj_detentor(), Types.VARCHAR);
            ps.setObject(5, element.getCnpj_emitente(), Types.VARCHAR);
            ps.setObject(6, element.getNum_nota(), Types.VARCHAR);
            ps.setObject(7, element.getSerie(), Types.VARCHAR);
            ps.setObject(8, element.getData_emissao(), Types.VARCHAR);
            ps.setObject(9, element.getData_entrada(), Types.VARCHAR);
            ps.setObject(10, element.getCfop(), Types.VARCHAR);
            ps.setObject(11, element.getValor_total(), Types.VARCHAR);
            ps.setObject(12, element.getValor_ipi_destacado(), Types.VARCHAR);
            ps.setObject(13, element.getValor_ipi_creditado(), Types.VARCHAR);
            ps.setObject(14, element.getEspecie_cred(), Types.VARCHAR);
            ps.setObject(15, element.getDecendio(), Types.VARCHAR);
            ps.setObject(16, element.getMes_periodo(),Types.VARCHAR);
            ps.setObject(17, element.getAno_periodo(), Types.VARCHAR);
           


            if (ps.execute()) {
                try ( ResultSet rs = ps.getResultSet()) {
                    if (rs.next()) {
                        insert = true;
                    }
                }
            }
        }
        return insert;
    }

    @Override
    public boolean update(ConnectionFactoryPool connF, String schema,CadastroR15VO element) throws SQLException, NullPointerException {
        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("UPDATE %s.r15 set r15=?, cnpj_declarante=?, cnpj_sucedida=?, cnpj_detentor=?, cnpj_emitente=?, num_nota=?, serie=?, data_emissao=?, data_entrada=?, cfop=?, valor_total=?, valor_ipi_destacado=?, valor_ipi_credito=?, especie_cred, decendio, mes_periodo, ano_periodo, id=?", schema))) {
            ps.setObject(1, element.getR15(), Types.VARCHAR);
            ps.setObject(2, element.getCnpj_declarante(), Types.VARCHAR);
            ps.setObject(3, element.getCnpj_sucedida(), Types.VARCHAR);
            ps.setObject(4, element.getCnpj_detentor(), Types.VARCHAR);
            ps.setObject(5, element.getCnpj_emitente(), Types.VARCHAR);
            ps.setObject(6, element.getNum_nota(), Types.VARCHAR);
            ps.setObject(7, element.getSerie(), Types.VARCHAR);
            ps.setObject(8, element.getData_emissao(), Types.VARCHAR);
            ps.setObject(9, element.getData_entrada(), Types.VARCHAR);
            ps.setObject(10, element.getCfop(), Types.VARCHAR);
            ps.setObject(11, element.getValor_total(), Types.VARCHAR);
            ps.setObject(12, element.getValor_ipi_destacado(), Types.VARCHAR);
            ps.setObject(13, element.getValor_ipi_creditado(), Types.VARCHAR);
            ps.setObject(14, element.getEspecie_cred(), Types.VARCHAR);
            ps.setObject(15, element.getDecendio(), Types.VARCHAR);
            ps.setObject(16, element.getMes_periodo(), Types.VARCHAR);
            ps.setObject(17, element.getAno_periodo(),Types.VARCHAR);
            ps.setObject(18, element.getId(), Types.BIGINT);

            return ps.executeUpdate() > 0;
        }
    }

   
        
    @Override
    public boolean delete(ConnectionFactoryPool connF, String schema,CadastroR15VO element) throws SQLException, NullPointerException {
        boolean deleted = false;

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(String.format("DELETE FROM %s.r15 where id=?", schema))) {
            ps.setLong(1, element.getId());

            deleted = ps.executeUpdate() > 0;
        }

        return deleted;
    }

    @Override
    public boolean exist(ConnectionFactoryPool connF, String schema, Long id) throws SQLException, NullPointerException {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CadastroR15VO> getAll(ConnectionFactoryPool connF, String schema, Object object) throws SQLException {
        return get(connF, schema, new ArrayList<>());
    }

    @Override
    public List<CadastroR15VO> get(ConnectionFactoryPool connF, String schema, List<FilterVO> filterObject) throws SQLException {
        List<CadastroR15VO> listComp = new ArrayList<>();
        QueryFilterVO q = transformFilter(filterObject);
        String query = String.format("select * from %s.r15 %s", schema, q.getParams());

        try ( Connection conn = connF.getConnection();  PreparedStatement ps = conn.prepareStatement(query)) {
            for (Map.Entry<Integer, Object> entry : q.getValues().entrySet()) {
                ps.setObject(entry.getKey(), entry.getValue());
            }
            try ( ResultSet rs = ps.executeQuery()) {
                while (rs.next()) {
                    CadastroR15VO comp = new CadastroR15VO();
                    comp.setId(rs.getLong("id"));
                    comp.setR15(rs.getString("r15"));
                    comp.setCnpj_declarante(rs.getString("cnpj_declarante"));
                    comp.setCnpj_sucedida(rs.getString("cnpj_sucedida"));
                    comp.setCnpj_detentor(rs.getString("cnpj_detentor"));
                    comp.setCnpj_emitente(rs.getString("cnpj_emitente"));                    
                    comp.setNum_nota(rs.getString("num_nota"));
                    comp.setSerie(rs.getString("serie"));
                    comp.setData_emissao(rs.getString("data_emissao"));
                    comp.setData_entrada(rs.getString("data_entrada"));
                    comp.setCfop(rs.getString("cfop"));
                    comp.setValor_total(rs.getString("valor_total"));
                    comp.setValor_ipi_destacado(rs.getString("valor_ipi_destacado"));
                    comp.setValor_ipi_creditado(rs.getString("Valor_ipi_creditado"));
                    comp.setEspecie_cred(rs.getString("especie_cred"));
                    comp.setDecendio(rs.getString("decendio"));
                    comp.setMes_periodo(rs.getString("mes_periodo"));
                    comp.setValor_ipi_destacado(rs.getString("ano_periodo"));
                   
                    
                    
                   

                    listComp.add(comp);
                }
            }
        }

        return listComp;
    }

}

