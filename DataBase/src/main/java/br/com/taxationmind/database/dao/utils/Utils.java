/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao.utils;

import br.com.taxationmind.livros.vo.FilterVO;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;

/**
 *
 * @author William Brito
 */
public class Utils {

    private static Random rand;

    static {
        try {
            rand = SecureRandom.getInstanceStrong();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static long getRandomNumber() {
        return rand.nextLong();
    }

    public static List<FilterVO> paramConverter(Map<String, FilterVO> FILTER_MAP, Map<String, List<String>> params) {
        List<FilterVO> ret = new ArrayList<>();

        for (Map.Entry<String, List<String>> entry : params.entrySet()) {
            FilterVO f = FILTER_MAP.get(entry.getKey());
            if (f != null) {
                f.setValue(entry.getValue().get(0));
                ret.add(f);
            }
        }

        if (params.containsKey("limit")) {
            ret.add(new FilterVO("limit", Types.INTEGER, params.get("limit").get(0)));
        }

        return ret;
    }

    public static String decompress(byte[] compressed) {
        StringBuilder sb = new StringBuilder();
        try (ByteArrayInputStream bis = new ByteArrayInputStream(compressed); GZIPInputStream gis = new GZIPInputStream(bis); BufferedReader br = new BufferedReader(new InputStreamReader(gis, "UTF-8"))) {

            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, "Erro ao descompactar String", ex);
        }

        return sb.toString();
    }

}
