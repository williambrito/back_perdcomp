/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package br.com.taxationmind.database.dao.utils;

import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author William Brito
 */
public class DAOErrorConverter {
    public static String convertSQLException(SQLException ex) {
        String message;

        PostgresErrorCode error = PostgresErrorCode.fromCode(ex.getSQLState());
        message = "Código [" + error.getCode() + "] ";
        message += "[" + error.getDescricaoPtBr() + "]";

        if (error.getRegularExpression() != null) {
            Pattern pattern = Pattern.compile(error.getRegularExpression());
            Matcher m = pattern.matcher(ex.getMessage());
            if (m.find()) {
                String[] values = new String[error.getPosition().length];
                for (int p = 0; p < error.getPosition().length; p++) {
                    values[p] = m.group(error.getPosition()[p]);
                }
                message += String.format(error.getModel(), values);
            }
        } else {
            message += "[" + ex.getMessage() + "]";
        }

        return message;
    }

    
    
    
}
